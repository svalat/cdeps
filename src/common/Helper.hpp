/*****************************************************
			 PROJECT  : cdeps
			 VERSION  : 0.0.0-dev
			 DATE     : 10/2018
			 LICENSE  : CeCILL-C
*****************************************************/

#ifndef CDEPS_HELPER_HPP
#define CDEPS_HELPER_HPP

/********************  HEADERS  *********************/
//std
#include <list>
#include <string>
#include <vector>

/*********************  TYPES  **********************/
/** represent a file as list **/
typedef std::list<std::string> FileLines;
typedef std::list<std::string> StringList;

/********************  NAMESPACE  *******************/
namespace cdeps
{

/*********************  STRUCT  *********************/
/** Provide help function for basic operations **/
struct Helper
{
	static void loadFileLines(FileLines & out,const std::string & fname);
	static void dumpInFile(const std::string & content,const std::string & fname);
	static void loadStringLines(FileLines & out, const std::string & value);
	static std::string join(const StringList & lst,const std::string & separator);
	static StringList split(const std::string & value,char separator, bool keepEmpty = true);
	static StringList uniq(const StringList & values);
	static StringList vectorToList(const std::vector<std::string> & vec);
};

}

#endif //CDEPS_HELPER_HPP
