/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <gtest/gtest.h>
#include "../../portability/OS.hpp"
#include "../Helper.hpp"

/***************** USING NAMESPACE ******************/
using namespace cdeps;

/*******************  FUNCTION  *********************/
TEST(Helper,loadFileLines)
{
	FileLines content;
	Helper::loadFileLines(content,DATADIR "/data-file-1.txt");

	EXPECT_EQ(content.size(),5);

	const char * exp[5] = {
		"line1",
		"line2",
		"",
		"after empty",
		"last text"
	};

	int i = 0;
	for (auto it : content)
		EXPECT_EQ(it,exp[i++]) << "Line: " << i;
}

/*******************  FUNCTION  *********************/
TEST(Helper,loadStringLines)
{
	FileLines content;
	const std::string input = "test\ntest2\n\ntest3";

	Helper::loadStringLines(content,input);

	EXPECT_EQ(content.size(),4);

	const char * exp[4] = {
		"test",
		"test2",
		"",
		"test3",
	};

	int i = 0;
	for (auto it : content)
		EXPECT_EQ(it,exp[i++]) << "Line: " << i;
}

/*******************  FUNCTION  *********************/
TEST(Helper,dumpInFile)
{
	std::string fname = OS::mktemp("cdeps-test-%1.txt");
	Helper::dumpInFile("test-content",fname);

	FileLines content;
	Helper::loadFileLines(content,fname);

	EXPECT_EQ(content.size(),1);
	EXPECT_EQ(*content.begin(),"test-content");

	OS::unlink(fname);
}

/*******************  FUNCTION  *********************/
TEST(Helper,split)
{
	StringList lst = Helper::split("a,b,c,,d,",',',true);

	const char * exp[] = {
		"a",
		"b",
		"c",
		"",
		"d",
		"",
		"END"
	};

	int i = 0;
	for (auto it : lst)
		EXPECT_EQ(it,exp[i++]) << "Line: " << i;
}

/*******************  FUNCTION  *********************/
TEST(Helper,join)
{
	StringList lst = Helper::split("a,b,c,,d,",',',true);
	EXPECT_EQ(Helper::join(lst,":"),"a:b:c::d:");
}

/*******************  FUNCTION  *********************/
TEST(Helper,uniq)
{
	StringList lst = Helper::split("a,b,c,,d,d,e,a,b,c,,",',',true);
	lst = Helper::uniq(lst);
	EXPECT_EQ(Helper::join(lst,":"),"a:b:c::d:e");
}