/*****************************************************
			 PROJECT  : cdeps
			 VERSION  : 0.0.0-dev
			 DATE     : 10/2018
			 LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
//std
#include <fstream>
#include <algorithm>
//local
#include "Debug.hpp"
#include "Helper.hpp"

/***************** USING NAMESPACE ******************/
using namespace std;

/********************  NAMESPACE  *******************/
namespace cdeps
{

/*******************  FUNCTION  *********************/
void Helper::loadFileLines(FileLines & out,const std::string & fname)
{
	//open file
	ifstream file;
	try {
  		file.open(fname);
		assumeArg(file.is_open(),"Fail to open file %1 : %2").arg(fname).argStrErrno().end();
	} catch (std::exception & e) {
  		CDEPS_FATAL_ARG("Fail to open file %1 : %2").arg(fname).arg(e.what()).end();
	}
	
	//loop on lines
	string buffer;	
	while(std::getline(file,buffer))
		out.push_back(buffer);
}

/*******************  FUNCTION  *********************/
void Helper::loadStringLines(FileLines & out, const std::string & value)
{
	//buffer (this is only used in unit tests.... so ok for fixed size)
	char buffer[1024];

	//loop
	int bufCursor = 0;
	for (size_t i = 0 ; i < value.size() ; i++) {
		if (value[i] == '\n') {
			buffer[bufCursor] = '\0';
			out.push_back(buffer);
			bufCursor = 0;
		} else {
			buffer[bufCursor++] = value[i];
		}
	}

	//flush
	buffer[bufCursor] = '\0';
	out.push_back(buffer);
}

/*******************  FUNCTION  *********************/
StringList Helper::split(const std::string & value, char separator, bool keepEmpty)
{
	//buffer
	std::string buf;
	StringList list;

	//loop
	for (size_t i = 0 ; i < value.size() ; ++i) {
		//if sep
		if (value[i] == separator) {
			if (buf.empty() == false || keepEmpty)
				list.push_back(buf);
			buf.clear();
		} else {
			//accumuate
			buf += value[i];
		}
	}

	//flash
	if (buf.empty() == false || keepEmpty)
		list.push_back(buf);

	return list;
}

/*******************  FUNCTION  *********************/
void Helper::dumpInFile(const std::string & content,const std::string & fname)
{
	//open file
	ofstream file;
	try {
  		file.open(fname);
		assumeArg(file.is_open(),"Fail to open file %1 : %2").arg(fname).argStrErrno().end();
	} catch (std::exception & e) {
  		CDEPS_FATAL_ARG("Fail to open file %1 : %2").arg(fname).arg(e.what()).end();
	}
	
	//loop on lines
	string buffer;	
	file << content;

	//close
	file.close();
}

/*******************  FUNCTION  *********************/
StringList Helper::vectorToList(const std::vector<std::string> & vec)
{
	StringList out;
	for (auto & it : vec)
		out.push_back(it);
	return out;
}

/*******************  FUNCTION  *********************/
std::string Helper::join(const StringList & lst,const std::string & separator)
{
	//var
	std::string out;

	//loop
	for (auto & it : lst) {
		if (out.empty() == false)
			out += separator;
		out += it;
	}

	//ok
	return out;
}

/*******************  FUNCTION  *********************/
StringList Helper::uniq(const StringList & values)
{
	//vars
	StringList out;

	//loop
	for (auto & it : values) {
		if (std::find(out.begin(), out.end(), it) == out.end()) {
			out.push_back(it);
		}
	}

	//ok
	return out;
}

}
