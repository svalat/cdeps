/*****************************************************
			 PROJECT  : cdeps
			 VERSION  : 0.0.0-dev
			 DATE     : 10/2018
			 LICENSE  : CeCILL-C
*****************************************************/

/** This file provide the glue to call daqpipe debug functions **/

#ifndef CDEPS_DEBUG_HPP
#define CDEPS_DEBUG_HPP

/********************  HEADERS  *********************/
#include <from-cern-lhcb-daqpipe-v2/Debug.hpp>

/********************  NAMESPACE  *******************/
namespace cdeps
{

/********************  MACROS  **********************/
#define CDEPS_FATAL(x)   DAQ_FATAL(x)
#define CDEPS_DEBUG(cat,x) DAQ_DEBUG(cat,x)
#define CDEPS_ERROR(x)     DAQ_ERROR(x)
#define CDEPS_WARNING(x)   DAQ_WARNING(x)
#define CDEPS_MESSAGE(x)   DAQ_MESSAGE(x)
#define CDEPS_INFO(x)      DAQ_INFO(x)

/********************  MACROS  **********************/
#define CDEPS_FATAL_ARG(x)     DAQ_FATAL_ARG(x)
#define CDEPS_ERROR_ARG(x)     DAQ_ERROR_ARG(x)
#define CDEPS_WARNING_ARG(x)   DAQ_WARNING_ARG(x)
#define CDEPS_MESSAGE_ARG(x)   DAQ_MESSAGE_ARG(x)
#define CDEPS_INFO_ARG(x)      DAQ_INFO_ARG(x)
#define CDEPS_DEBUG_ARG(cat,x) DAQ_DEBUG_ARG(cat,x)

/********************  MACROS  **********************/
#define DAQ_TO_STRING(x) #x
#ifdef NDEBUG
	#define CDEPS_ASSERT(x)      do{} while(0)
#else
	#define CDEPS_ASSERT(x)      do{ if (!(x)) DAQ::Debug(DAQ_TO_STRING(x),DAQ_CODE_LOCATION,DAQ::MESSAGE_ASSERT).end(); } while(0)
#endif

}

#endif //CDEPS_DEBUG_HPP
