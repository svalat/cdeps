/*****************************************************
			 PROJECT  : cdeps
			 VERSION  : 0.0.0-dev
			 DATE     : 11/2018
			 LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <cstdlib>
#include <iostream>
#include "core/Options.hpp"
#include "runner/RunnerChecker.hpp"
#include "common/Debug.hpp"

/***************** USING NAMESPACE ******************/
using namespace cdeps;
using namespace std;

/*******************  FUNCTION  *********************/
int main(int argc, char ** argv)
{
	//debug
	//DAQ::Debug::enableAll();

	//load options
	Options options;
	options.loadEnv();
	options.parseArgs(argc,argv);

	//load file
	FileLines content;
	Helper::loadFileLines(content,options.filename);

	//parse and convert to spec
	SpecFile spec;
	spec.loadContent(content,options.filename);

	//load into project
	Project project(&options);
	project.loadSpec(spec);

	//vars
	bool status = false;

	//apply generators
	switch(options.generator)
	{
		case CDEPS_GENERATOR_TEXT:
			do {
				RunnerChecker runner;
				status = runner.runOnProject(project);
				runner.displayStatus(project);
				cout << "---------------------- VARIABLES ------------------------" << endl;
				runner.printVariables(project);
				cout << "---------------------------------------------------------" << endl;
				cout << "------------------------ HINTS --------------------------" << endl;
				runner.printHints(project);
				cout << "---------------------------------------------------------" << endl;
			} while (0);
			break;
		default:
			CDEPS_FATAL_ARG("Invalid generator, not yet supported : %1").arg(options.generator).end();
			break;
	}

	//final status
	if (status)
		return EXIT_SUCCESS;
	else
		return EXIT_FAILURE;
}
