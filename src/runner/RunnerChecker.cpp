/*****************************************************
			 PROJECT  : cdeps
			 VERSION  : 0.0.0-dev
			 DATE     : 11/2018
			 LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <iostream>
#include <algorithm>
#include <iostream>
#include "RunnerChecker.hpp"
#include "../common/Debug.hpp"
#include "../portability/Path.hpp"
#include "../portability/FsLayout.hpp"
#include "../portability/OS.hpp"

/********************  MACRO  ***********************/
#define COLOR_NORMAL   "\033[0m"
#define COLOR_RED      "\033[31m"
#define COLOR_GREEN    "\033[32m"
#define COLOR_YELLOW   "\033[33m"
#define COLOR_BLUE     "\033[34m"
#define COLOR_MAGENTA  "\033[35m"
#define COLOR_CYAN     "\033[36m"
#define COLOR_WHITE    "\033[37m"

/***************** USING NAMESPACE ******************/
using namespace std;

/********************  NAMESPACE  *******************/
namespace cdeps
{

/********************  CONSTS  **********************/
const char * cstSymbolSourceCheckCodeCLang[] = {
//TYPE
"\
#include <stdlib.h>\n\
#include <%1>\n\
\n\
int main(void) {\n\
	%2 * tmp = (%2*)NULL;\n\
}\n\
",
//FUNCTION
"\
#include <%1>\n\
\n\
int main(void) {\n\
	void * tmp = (void*)%2;\n\
}\n\
",
//MACRO
"\
#include <%1>\n\
\n\
#ifndef %2\n\
	#error Missing macro %2\n\
#endif\n\
",
//VARIABLE
"\
#include <%1>\n\
\n\
int main(void) {\n\
	void * ptr = (void*)&%2;\n\
}\n\
"
};
const char ** cstSymbolSourceCheckCodeCPPLang = cstSymbolSourceCheckCodeCLang;
const char * cstSymbolSourceCheckLibCodeCLang = "\
void %1(void);\n\
\n\
int main(void)\n\
{\n\
	%1();\n\
}\n\
";

/*******************  FUNCTION  *********************/
RunnerChecker::RunnerChecker(void)
{

}

/*******************  FUNCTION  *********************/
bool RunnerChecker::runOnProject(Project & project)
{
	//var
	bool result = true;
	this->project = &project;

	//loop on packages
	for (auto & pack : project.getPackageList()) {
		//check
		if (checkPackage(pack) == false && pack.optional == false) {
			result = false;
		}
	}

	//build variables
	this->buildVariables();

	//return
	return result;
}

/*******************  FUNCTION  *********************/
bool RunnerChecker::checkPackage(Package & package)
{
	//vars
	bool result = true;
	bool hasOneNonOpt = false;

	//loop on all files
	for (auto & file : package.files) {
		if (checkFile(file) == false && file.optional == false) {
			result = false;
		}
		if (file.optional == false) {
			hasOneNonOpt = true;
		}
	}

	//if has nothing found, consider not here
	if (hasOneNonOpt == false && package.files.empty() == false) {
		result = false;
		package.optional = true;
	}

	//status
	package.found = result;
	return result;
}

/*******************  FUNCTION  *********************/
bool RunnerChecker::isIncludeFile(const std::string & fname)
{
	return (fname.substr(fname.size()-2,2) == ".h"
		|| fname.substr(fname.size()-2,2) == ".H"
		|| fname.substr(fname.size()-4,4) == ".hpp"
		|| fname.substr(fname.size()-4,4) == ".xx"
		|| fname.substr(0,8) == "/include/");
}

/*******************  FUNCTION  *********************/
bool RunnerChecker::isLib(const std::string & file)
{
	return (file.substr(0,5) == "/lib/");
}


/*******************  FUNCTION  *********************/
bool RunnerChecker::checkFile(PackageExpectedFile & file)
{
	//vars
	bool result = false;

	//split path parts
	PathList paths = FsLayout::getAbsolute(file.path, FsLayout::getDefaultPrefixes());

	//add extra search paths
	for (auto & it : file.extraSearchPaths)
		paths.push_back(it);

	//make list
	if (file.path.substr(0,5) == "/lib/") {
		for (auto & it : paths) {
			it.getMut(-1) = FsLayout::getDynLibFilename(it.get(-1));
		}
	}

	//gen list of full paths to search
	paths = expandOptionalSubPaths(paths);

	//reverse order
	paths.reverse();

	//search and stop on first
	for (auto & it : paths) {
		std::string fname = it.toString();
		if (OS::fileExists(fname)) {
			//mark ok
			file.found = true;
			file.foundAt = fname;
			result = true;

			//search inner symbols
			for (auto & symIt : file.symbols) {
				if (isIncludeFile(file.path)) {
					if (checkIncludeSymbol(file,symIt) == false && symIt.optional == false)
						result = false;
				} else if (isLib(file.path)) {
					if (checkLibSymbol(file,symIt) == false && symIt.optional == false)
						result = false;
				} else {
					CDEPS_FATAL("Unsupported symbol search in file type !");
				}
			}

			//finish
			break;
		}
	}

	//ret
	file.found = result;
	return result;
}

/*******************  FUNCTION  *********************/
bool RunnerChecker::checkLibSymbol(const PackageExpectedFile & file, PackExpectedFileSymbol & symbol)
{
	//checks
	assumeArg(symbol.type == CDEPS_SYMBOL_FUNCTION,"Invalid symbol type : %1 at %2").arg(symbol.type).arg(symbol.source).end();
	assumeArg(symbol.lang == CDEPS_LANG_C,"Invalid symbol language : %1 at %2").arg(symbol.type).arg(symbol.source).end();

	//path
	Path path(file.path);
	std::string libfname = path.pop();

	//build code
	std::string code = DAQ::FormattedMessage(cstSymbolSourceCheckLibCodeCLang).arg(symbol.name).toString();

	//write into tmpfile
	std::string fname = OS::mktemp("cdeps-include-symbol-%1.c");
	Helper::dumpInFile(code,fname);

	//OS::system(std::string("cat ")+fname,false);

	//build command line
	std::string cmd = DAQ::FormattedMessage("gcc -L%1 -l%2 -o /dev/null %3").arg(path).arg(libfname).arg(fname).toString();
	CDEPS_DEBUG_ARG("runner-checker","Search include symbol : %1").arg(cmd).end();
	bool status = OS::system(cmd,true);

	//remove
	OS::unlink(fname);

	//ok
	symbol.found = status;
	return status;
}

/*******************  FUNCTION  *********************/
bool RunnerChecker::checkIncludeSymbol(const Path & file, const std::string & symbolName,PackageSymbolLang lang,PackageSymbolType type)
{
	//check
	assumeArg(type >= 0 && type < CDEPS_SYMBOL_MAX_ID,"Invalid symbol type : %1").arg(type).end();

	//fname (remove ./)
	std::string incfname = file.toString();
	if (file.isAbsolute() == false)
		incfname = incfname.substr(2,incfname.size() - 2);

	//gen code
	std::string code;
	switch(lang) {
		case CDEPS_LANG_C:
			code = DAQ::FormattedMessage(cstSymbolSourceCheckCodeCLang[type]).arg(incfname).arg(symbolName).toString();
			break;
		case CDEPS_LANG_CPP:
			code = DAQ::FormattedMessage(cstSymbolSourceCheckCodeCPPLang[type]).arg(incfname).arg(symbolName).toString();
			break;
		default:
			CDEPS_FATAL_ARG("Invalid language : %1").arg(lang).end();
	}

	//write into tmpfile
	std::string fname = OS::mktemp("cdeps-include-symbol-%1.c");
	Helper::dumpInFile(code,fname);

	//UnixOS::system(std::string("cat ")+fname,false);

	//try to compile
	std::string cmd;
	switch(lang) {
		case CDEPS_LANG_C:
			cmd = DAQ::FormattedMessage("gcc -c -o /dev/null %1").arg(fname).toString();
			break;
		case CDEPS_LANG_CPP:
			cmd = DAQ::FormattedMessage("g++ -c -o /dev/null %1").arg(fname).toString();
			break;
		default:
			CDEPS_FATAL_ARG("Invalid language : %1").arg(lang).end();
	}

	//run
	CDEPS_DEBUG_ARG("runner-checker","Search include symbol : %1").arg(cmd).end();
	bool status = UnixOS::system(cmd,true);

	//remove
	OS::unlink(fname);

	//ret
	return status;
}

/*******************  FUNCTION  *********************/
bool RunnerChecker::checkIncludeSymbol(const PackageExpectedFile & file, PackExpectedFileSymbol & symbol)
{
	//vars
	bool result = true;

	//check
	assume(file.found && file.foundAt.empty() == false, "Get an unfound file !");

	//if found
	if (checkIncludeSymbol(file.foundAt,symbol.name,symbol.lang,symbol.type))
		symbol.found = true;
	else if (symbol.optional == false)
		result = false;
	

	//ret
	return result;
}

/*******************  FUNCTION  *********************/
PathList RunnerChecker::expandOptionalSubPaths(const PathList & paths)
{
	//var
	PathList res;

	//loop
	for (auto & path : paths) {
		PathList tmp = expandOptionalSubPaths(path);
		res.splice(res.end(),tmp);
	}

	//final
	return res;
}

/*******************  FUNCTION  *********************/
PathList RunnerChecker::expandOptionalSubPaths(Path path)
{
	//vars
	PathList ret;

	//loop on all
	for (int i = 0 ;i < path.size() ; i++) {
		//extract
		std::string & elt = path.getMutable(i);

		//if option
		if (elt[0] == '[' && elt[elt.size() - 1] == ']') {
			//build list
			std::string pattern = elt.substr(1,elt.size() - 2);
			StringList lst = Helper::split(pattern,',',true);

			//build each
			for (auto & it : lst) {
				elt = it;
				PathList tmp = expandOptionalSubPaths(path);
				for (auto & it2 : tmp)
					ret.push_back(it2);
			}

			//do not continue
			return ret;
		}
	}

	//just add path
	ret.push_back(path);
	return ret;
}

/*******************  FUNCTION  *********************/
static const char * getColor(bool found, bool optional)
{
	if (found) {
		return COLOR_GREEN;
	} else {
		if (optional)
			return COLOR_YELLOW;
		else
			return COLOR_RED;
	}
}

/*******************  FUNCTION  *********************/
void RunnerChecker::printVariables(const Project & project)
{
	//loop on all
	for (auto & var : project.getVariables()) {
		if (var.second.empty() == false) {
			cout << var.first << '=' << var.second << endl;
		}
	}
}

/*******************  FUNCTION  *********************/
void RunnerChecker::displayStatus(const Project & project)
{
	//var
	bool global = true;

	//loops on elements
	for (auto & pack : project.getPackageList()) {
		const char * packColor = getColor(pack.found,pack.optional);
		std::cout << packColor << pack.name << COLOR_NORMAL << ":" << std::endl;

		if (pack.found == false && pack.optional == false)
			global = false;

		for (auto & file : pack.files) {
			const char * fileColor = getColor(file.found,file.optional);
			if (file.foundAt.empty() == false)
				std::cout << "   - " << fileColor << file.foundAt << COLOR_NORMAL << std::endl;
			else
				std::cout << "   - " << fileColor << file.path << COLOR_NORMAL << std::endl;

			for (auto & sym : file.symbols) {
				const char * symColor = getColor(sym.found,sym.optional);
				std::cout << "      + "  << symColor << sym.name << COLOR_NORMAL << std::endl;
			}
		}
	}

	//final status
	std::cout << std::endl;
	if (global)
		std::cout << COLOR_GREEN << "Status OK" << COLOR_NORMAL << std::endl;
	else
		std::cout << COLOR_RED << "Missing dependencies" << COLOR_NORMAL << std::endl;
}

/*******************  FUNCTION  *********************/
void RunnerChecker::buildVariables(void)
{
	//loop
	for (auto & pack : project->getPackageList()) {
		//skip if not found
		if (pack.found) {
			//vars
			StringList includeDirs;
			StringList libDirs;
			StringList libraries;
			StringList libfiles;

			//setup vars
			for (auto & v : pack.setVars)
				project->setVariable(v.name,pack.name,v.append);

			//loop on all files
			for (auto & file : pack.files) {
				//skip if not found
				if (file.found) {
					//set
					for (auto & v : file.setVars)
						project->setVariable(v.name,file.foundAt,v.append);
					
					//extract dir
					Path dir(file.foundAt);
					dir.pop();

					//auto vars
					if (isIncludeFile(file.path)) {
						includeDirs.push_back(dir.toString());
					} else if (isLib(file.path)) {
						libraries.push_back(file.foundAt);
						libDirs.push_back(dir.toString());
						libfiles.push_back(Path(file.path).get(-1));
					}

					//loop on all symboles in file
					for (auto & sym : file.symbols) {
						if (sym.found) {
							//set
							for (auto & v : sym.setVars)
								project->setVariable(v.name,sym.name,v.append);
						}
					}
				}
			}

			//make uniq
			includeDirs = Helper::uniq(includeDirs);
			libDirs = Helper::uniq(libDirs);
			libraries = Helper::uniq(libraries);

			//auto
			project->setVariable(genVariableName(pack.name,"FOUND"),"YES");
			project->setVariable(genVariableName(pack.name,"INCLUDE_DIRS"),Helper::join(includeDirs,":"));
			project->setVariable(genVariableName(pack.name,"LIBRARY_DIRS"),Helper::join(libDirs,":"));
			project->setVariable(genVariableName(pack.name,"LIBRARIES"),Helper::join(libraries,":"));
			if (includeDirs.empty() == false)
				project->setVariable(genVariableName(pack.name,"CFLAGS"),std::string("-I")+Helper::join(includeDirs," -I"));
			if (libDirs.empty() == false) {
				project->setVariable(genVariableName(pack.name,"LDFLAGS"),std::string("-L")+Helper::join(libDirs," -L")+std::string(" -l")+Helper::join(libfiles," -l"));
				project->setVariable(genVariableName(pack.name,"RPATH_LDFLAGS"),std::string("-Wl,-rpath,")+Helper::join(libDirs," -Wl,-rpath,"));
			}
		}
	}
}

/*******************  FUNCTION  *********************/
std::string RunnerChecker::genVariableName(const std::string & packageName, const std::string & second, const std::string & third)
{
	//checks
	assume(packageName.empty() == false, "Invalid empty package name");

	//start build
	std::string name = packageName;

	//add second
	name += "_";
	name += second;

	//add third if has one
	if (third.empty() == false) {
		name += "_";
		name += third;
	}

	//setup to upper case
	std::transform(name.begin(), name.end(),name.begin(), ::toupper);

	//replace '-'
	for (auto & c : name)
		if (c == '-')
			c = '_';

	//ok
	return name;
}

/*******************  FUNCTION  *********************/
void RunnerChecker::printHints(const Project & project)
{
	//loop
	for (auto & pack : project.getPackageList()) {
		//skip if not found
		if (pack.found == false) {
			const char * colorIn = getColor(pack.found,pack.optional);
			cout << colorIn << "Failed to found '" << pack.name << "'." << COLOR_NORMAL << endl;
			if (pack.infos.brief.empty() == false)
				cout << colorIn << "    - " << pack.infos.brief << COLOR_NORMAL << endl;
			if (pack.infos.url.empty() == false)
				cout << colorIn << "    - " << pack.infos.url << COLOR_NORMAL << endl;
		}
	}
}

}
