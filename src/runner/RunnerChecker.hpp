/*****************************************************
			 PROJECT  : cdeps
			 VERSION  : 0.0.0-dev
			 DATE     : 11/2018
			 LICENSE  : CeCILL-C
*****************************************************/

#ifndef CDEPS_RUNNER_CHECKER_HPP
#define CDEPS_RUNNER_CHECKER_HPP

/********************  HEADERS  *********************/
#include "../portability/Path.hpp"
#include "../core/Project.hpp"
#include "../core/Options.hpp"

/********************  NAMESPACE  *******************/
namespace cdeps
{

/*********************  CLASS  **********************/
class RunnerChecker
{
	public:
		RunnerChecker(void);
		bool runOnProject(Project & project);
		void displayStatus(const Project & project);
		void printVariables(const Project & project);
		void printHints(const Project & project);
	public:
		static PathList expandOptionalSubPaths(Path path);
		static PathList expandOptionalSubPaths(const PathList & paths);
		static bool checkIncludeSymbol(const Path & file, const std::string & symbolName, PackageSymbolLang lang, PackageSymbolType type);
		static std::string genVariableName(const std::string & packageName, const std::string & second, const std::string & third = "");
	private:
		static bool isIncludeFile(const std::string & fname);
		static bool isLib(const std::string & file);
	private:
		bool checkPackage(Package & package);
		bool checkFile(PackageExpectedFile & file);
		bool checkIncludeSymbol(const PackageExpectedFile & file, PackExpectedFileSymbol & symbol);
		bool checkLibSymbol(const PackageExpectedFile & file, PackExpectedFileSymbol & symbol);
		void buildVariables(void);
	private:
		Project * project;
};

}

#endif //CDEPS_RUNNER_CHECKER_HPP
