/*****************************************************
			 PROJECT  : cdeps
			 VERSION  : 0.0.0-dev
			 DATE     : 11/2018
			 LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <gtest/gtest.h>
#include "../RunnerChecker.hpp"

/********************  NAMESPACE  *******************/
using namespace cdeps;

/*******************  FUNCTION  *********************/
TEST(TestRunnerChecker, expandOptionalSubPaths_none)
{
	Path path("/usr/local/lib/libc.so");
	PathList lst = RunnerChecker::expandOptionalSubPaths(path);
	EXPECT_EQ(lst.size(),1);

	const char * ref[] = {
		"/usr/local/lib/libc.so",
		"END"
	};

	int i = 0;
	for (auto & it : lst) {
		EXPECT_EQ(it.toString(), ref[i++]);
	}
}

/*******************  FUNCTION  *********************/
TEST(TestRunnerChecker, expandOptionalSubPaths_one)
{
	Path path("/usr/local/[lib,lib64,]/libc.so");
	PathList lst = RunnerChecker::expandOptionalSubPaths(path);
	EXPECT_EQ(lst.size(),3);

	const char * ref[] = {
		"/usr/local/lib/libc.so",
		"/usr/local/lib64/libc.so",
		"/usr/local/libc.so",
		"END"
	};

	int i = 0;
	for (auto & it : lst) {
		EXPECT_EQ(it.toString(), ref[i++]);
	}
}

/*******************  FUNCTION  *********************/
TEST(TestRunnerChecker, expandOptionalSubPaths_two)
{
	Path path("/usr/local/[lib,lib64,]/[anotherA,anotherB]/libc.so");
	PathList lst = RunnerChecker::expandOptionalSubPaths(path);
	EXPECT_EQ(lst.size(),6);

	const char * ref[] = {
		"/usr/local/lib/anotherA/libc.so",
		"/usr/local/lib/anotherB/libc.so",
		"/usr/local/lib64/anotherA/libc.so",
		"/usr/local/lib64/anotherB/libc.so",
		"/usr/local/anotherA/libc.so",
		"/usr/local/anotherB/libc.so",
		"END"
	};

	int i = 0;
	for (auto & it : lst) {
		EXPECT_EQ(it.toString(), ref[i++]);
	}
}

/*******************  FUNCTION  *********************/
TEST(TestRunnerChecker, expandMultiple)
{
	PathList paths;
	paths.push_back(Path("/usr/local/[lib,lib64,]/[anotherA,anotherB]/libc.so"));
	paths.push_back(Path("/usr/include/[zmq,]/zmq.h"));
	paths.push_back(Path("/usr/local/include/[zmq,]/zmq.h"));
	PathList lst = RunnerChecker::expandOptionalSubPaths(paths);
	EXPECT_EQ(lst.size(),10);

	const char * ref[] = {
		"/usr/local/lib/anotherA/libc.so",
		"/usr/local/lib/anotherB/libc.so",
		"/usr/local/lib64/anotherA/libc.so",
		"/usr/local/lib64/anotherB/libc.so",
		"/usr/local/anotherA/libc.so",
		"/usr/local/anotherB/libc.so",
		"/usr/include/zmq/zmq.h",
		"/usr/include/zmq.h",
		"/usr/local/include/zmq/zmq.h",
		"/usr/local/include/zmq.h",
		"END"
	};

	int i = 0;
	for (auto & it : lst) {
		EXPECT_EQ(it.toString(), ref[i++]);
	}
}

/*******************  FUNCTION  *********************/
TEST(TestRunnerChecker, checkSymbol_C)
{
	EXPECT_TRUE(RunnerChecker::checkIncludeSymbol(Path("stdio.h"),"printf", CDEPS_LANG_C, CDEPS_SYMBOL_FUNCTION));
	EXPECT_FALSE(RunnerChecker::checkIncludeSymbol(Path("stdio.h"),"printf", CDEPS_LANG_C, CDEPS_SYMBOL_TYPE));
	EXPECT_TRUE(RunnerChecker::checkIncludeSymbol(Path("stdio.h"),"size_t", CDEPS_LANG_C, CDEPS_SYMBOL_TYPE));
	EXPECT_TRUE(RunnerChecker::checkIncludeSymbol(Path("errno.h"),"errno", CDEPS_LANG_C, CDEPS_SYMBOL_VARIABLE));
	EXPECT_FALSE(RunnerChecker::checkIncludeSymbol(Path("stdio.h"),"toto", CDEPS_LANG_C, CDEPS_SYMBOL_FUNCTION));
}

/*******************  FUNCTION  *********************/
TEST(TestRunnerChecker, checkSymbol_CPP)
{
	EXPECT_TRUE(RunnerChecker::checkIncludeSymbol(Path("stdio.h"),"printf", CDEPS_LANG_CPP, CDEPS_SYMBOL_FUNCTION));
	EXPECT_FALSE(RunnerChecker::checkIncludeSymbol(Path("stdio.h"),"printf", CDEPS_LANG_CPP, CDEPS_SYMBOL_TYPE));
	EXPECT_TRUE(RunnerChecker::checkIncludeSymbol(Path("stdio.h"),"size_t", CDEPS_LANG_CPP, CDEPS_SYMBOL_TYPE));
	EXPECT_TRUE(RunnerChecker::checkIncludeSymbol(Path("errno.h"),"errno", CDEPS_LANG_CPP, CDEPS_SYMBOL_VARIABLE));
	EXPECT_FALSE(RunnerChecker::checkIncludeSymbol(Path("stdio.h"),"toto", CDEPS_LANG_CPP, CDEPS_SYMBOL_FUNCTION));
}

/*******************  FUNCTION  *********************/
TEST(TestRunnerChecker, full_include_stdio_funcs)
{
	FileLines content;
	content.push_back("stdio:");
	content.push_back("\t/include/[truc,]/stdio.h:");
	content.push_back("\t\tprintf");
	content.push_back("\t\tprintfoops:");
	content.push_back("\t\t\t[optional]");
	content.push_back("\t\tfopen");
	content.push_back("\t\tfclose");

	SpecFile spec;
	spec.loadContent(content,"none.none");

	Options options;
	Project project(&options);
	project.loadSpec(spec);

	RunnerChecker runner;
	EXPECT_TRUE(runner.runOnProject(project));
}


/*******************  FUNCTION  *********************/
TEST(TestRunnerChecker, full_include_stdio_funcs_fail_file)
{
	FileLines content;
	content.push_back("stdio:");
	content.push_back("\t/include/[truc,]/oops-stdio.h:");
	content.push_back("\t\tprintf");
	content.push_back("\t\tfopen");
	content.push_back("\t\tfclose");

	SpecFile spec;
	spec.loadContent(content,"none.none");

	Options options;
	Project project(&options);
	project.loadSpec(spec);

	RunnerChecker runner;
	EXPECT_FALSE(runner.runOnProject(project));
}

/*******************  FUNCTION  *********************/
TEST(TestRunnerChecker, full_include_stdio_funcs_fail_symb)
{
	FileLines content;
	content.push_back("stdio:");
	content.push_back("\t/include/[truc,]/oops-stdio.h:");
	content.push_back("\t\tprintfoops");
	content.push_back("\t\tfopen");
	content.push_back("\t\tfclose");

	SpecFile spec;
	spec.loadContent(content,"none.none");

	Options options;
	Project project(&options);
	project.loadSpec(spec);

	RunnerChecker runner;
	EXPECT_FALSE(runner.runOnProject(project));
}

/*******************  FUNCTION  *********************/
TEST(TestRunnerChecker, full_lib_pthread)
{
	FileLines content;
	content.push_back("pthread:");
	content.push_back("\t/lib/pthread:");
	content.push_back("\t\tpthread_create");
	content.push_back("\t\tpthread_exit");
	content.push_back("\t\tbob_sponge:");
	content.push_back("\t\t\t[optional]");

	SpecFile spec;
	spec.loadContent(content,"none.none");

	Options options;
	Project project(&options);
	project.loadSpec(spec);

	RunnerChecker runner;
	EXPECT_TRUE(runner.runOnProject(project));
}


/*******************  FUNCTION  *********************/
TEST(TestRunnerChecker, full_lib_pthread_invalid_lib)
{
	FileLines content;
	content.push_back("stdio:");
	content.push_back("\t/lib/pthread-not-found:");
	content.push_back("\t\tpthread_create");
	content.push_back("\t\tpthread_exit");
	content.push_back("\t\tbob_sponge:");
	content.push_back("\t\t\t[optional]");

	SpecFile spec;
	spec.loadContent(content,"none.none");

	Options options;
	Project project(&options);
	project.loadSpec(spec);

	RunnerChecker runner;
	EXPECT_FALSE(runner.runOnProject(project));
}

/*******************  FUNCTION  *********************/
TEST(TestRunnerChecker, full_lib_pthread_invalid_sym)
{
	FileLines content;
	content.push_back("stdio:");
	content.push_back("\t/lib/pthread:");
	content.push_back("\t\tpthread_create");
	content.push_back("\t\tpthread_exit");
	content.push_back("\t\tbob_sponge");

	SpecFile spec;
	spec.loadContent(content,"none.none");

	Options options;
	Project project(&options);
	project.loadSpec(spec);

	RunnerChecker runner;
	EXPECT_FALSE(runner.runOnProject(project));
}

/*******************  FUNCTION  *********************/
TEST(TestRunnerChecker, share)
{
	Options options;
	options.prefixes.push_back(DATADIR);

	FileLines content;
	content.push_back("stdio:");
	content.push_back("\t/share/test/file.txt");

	SpecFile spec;
	spec.loadContent(content,"none.none");

	Project project(&options);
	project.loadSpec(spec);

	RunnerChecker runner;
	EXPECT_FALSE(runner.runOnProject(project));
}

/*******************  FUNCTION  *********************/
TEST(TestRunnerChecker, etc)
{
	Options options;
	options.prefixes.push_back(DATADIR);

	FileLines content;
	content.push_back("stdio:");
	content.push_back("\t/etc/config.json");

	SpecFile spec;
	spec.loadContent(content,"none.none");

	Project project(&options);
	project.loadSpec(spec);

	RunnerChecker runner;
	EXPECT_FALSE(runner.runOnProject(project));
}

/*******************  FUNCTION  *********************/
TEST(TestRunnerChecker, package_empty_1)
{
	FileLines content;
	content.push_back("pthread");
	
	SpecFile spec;
	spec.loadContent(content,"none.none");

	Options options;
	Project project(&options);
	project.loadSpec(spec);

	RunnerChecker runner;
	EXPECT_TRUE(runner.runOnProject(project));

	EXPECT_TRUE(project.getPackageList().front().found);
}

/*******************  FUNCTION  *********************/
TEST(TestRunnerChecker, package_empty_2)
{
	FileLines content;
	content.push_back("pthread:");
	content.push_back("\t/include/pthreadblablablabla.h:");
	content.push_back("\t\t[optional]");
	
	SpecFile spec;
	spec.loadContent(content,"none.none");

	Options options;
	Project project(&options);
	project.loadSpec(spec);

	RunnerChecker runner;
	EXPECT_TRUE(runner.runOnProject(project));

	EXPECT_FALSE(project.getPackageList().front().found);
}

/*******************  FUNCTION  *********************/
TEST(TestRunnerChecker, variables_1)
{
	FileLines content;
	content.push_back("pthread:");
	content.push_back("\t[set PACK_VAR]");
	content.push_back("\t/include/stdio.h:");
	content.push_back("\t\t[set-append MY_VAR_APPEND]");
	content.push_back("\t/include/pthread.h:");
	content.push_back("\t\t[set MY_VAR]");
	content.push_back("\t\t[set-append MY_VAR_APPEND]");
	content.push_back("\t\tpthread_create");
	content.push_back("\t\tpthread_exit");
	content.push_back("\t/lib/pthread:");
	content.push_back("\t\tpthread_create");
	content.push_back("\t\tpthread_exit:");
	content.push_back("\t\t\t[set PTHREAD_EXIT_SYM]");

	SpecFile spec;
	spec.loadContent(content,"none.none");

	Options options;
	Project project(&options);
	project.loadSpec(spec);

	RunnerChecker runner;
	EXPECT_TRUE(runner.runOnProject(project));

	VariableMap vars = project.getVariables();
	EXPECT_EQ(vars.size(),11);

	EXPECT_EQ(vars["PTHREAD_FOUND"],"YES");
	EXPECT_EQ(vars["PTHREAD_INCLUDE_DIRS"],"/usr/include");
	EXPECT_EQ(vars["PTHREAD_LIBRARIES"],"/usr/lib64/libpthread.so");
	EXPECT_EQ(vars["PTHREAD_LIBRARY_DIRS"],"/usr/lib64");
	EXPECT_EQ(vars["PTHREAD_CFLAGS"],"-I/usr/include");
	EXPECT_EQ(vars["PTHREAD_LDFLAGS"],"-L/usr/lib64 -lpthread");
	EXPECT_EQ(vars["PTHREAD_RPATH_LDFLAGS"],"-Wl,-rpath,/usr/lib64");
	EXPECT_EQ(vars["MY_VAR"],"/usr/include/pthread.h");
	EXPECT_EQ(vars["PTHREAD_EXIT_SYM"],"pthread_exit");
	EXPECT_EQ(vars["PACK_VAR"],"pthread");
	EXPECT_EQ(vars["MY_VAR_APPEND"],"/usr/include/stdio.h:/usr/include/pthread.h");
}
