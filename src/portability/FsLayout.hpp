/*****************************************************
			 PROJECT  : cdeps
			 VERSION  : 0.0.0-dev
			 DATE     : 11/2018
			 LICENSE  : CeCILL-C
*****************************************************/

#ifndef CDEPS_FS_LAYOUT_HPP
#define CDEPS_FS_LAYOUT_HPP

/********************  HEADERS  *********************/
#include "config.h"

/********************  HEADERS  *********************/
#ifdef PORTABILITY_OS_UNIX
	#include "unix/UnixFsLayout.hpp"
	namespace cdeps
	{
		typedef UnixFsLayout FsLayout;
	}
#else
	#error Unsupported portability mode for OS, should be UNIX
#endif

#endif //CDEPS_FS_LAYOUT_HPP
