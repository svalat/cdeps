/*****************************************************
			 PROJECT  : cdeps
			 VERSION  : 0.0.0-dev
			 DATE     : 11/2018
			 LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
//std
#include <cstdlib>
#include <cstring>
#include <iterator>

//unix
#include <unistd.h>
#include <libgen.h>

//current
#include "../../common/Debug.hpp"
#include "../OS.hpp"
#include "UnixPath.hpp"

/********************  NAMESPACE  *******************/
namespace cdeps
{

/*******************  FUNCTION  *********************/
UnixPath::UnixPath(const std::string path)
{
	this->elements = UnixPath::split(path);
	this->absolute = (path[0] == CDEPS_PATH_SEPARATOR_CHAR);
}

/*******************  FUNCTION  *********************/
UnixPath UnixPath::clone(void)
{
	return *this;
}

/*******************  FUNCTION  *********************/
UnixPath & UnixPath::append(const std::string & subPath)
{
	StringList tmp = UnixPath::split(subPath);
	for (auto & it : tmp) {
		this->elements.push_back(it);
	}
	return *this;
}

/*******************  FUNCTION  *********************/
UnixPath & UnixPath::append(const UnixPath & subPath)
{
	for (auto elt : subPath.elements) {
		this->elements.push_back(elt);
	}

	return *this;
}

/*******************  FUNCTION  *********************/
std::string UnixPath::pop(void)
{
	//check
	assume(elements.empty() == false,"Cannot pop on and empty path !");

	std::string tmp = this->elements.back();
	this->elements.pop_back();
	return tmp;
}

/*******************  FUNCTION  *********************/
std::string UnixPath::popRoot(void)
{
	//check
	assume(elements.empty() == false,"Cannot pop on and empty path !");

	std::string tmp = this->elements.front();
	this->elements.pop_front();

	this->absolute = false;

	return tmp;
}

/*******************  FUNCTION  *********************/
std::string & UnixPath::getMutable(int index)
{
	//valid
	assumeArg(index >= -(int)(elements.size()) && index < (int)elements.size(), "Invalid index (%1) to access path element of '%2")
		.arg(index)
		.arg(toString())
		.end();
	
	//order
	if (index >= 0) {
		auto it = elements.begin();
		std::advance(it,index);
		return *it;
	} else {
		auto it = elements.rbegin();
		std::advance(it,-index-1);
		return *it;
	}
}

/*******************  FUNCTION  *********************/
std::string & UnixPath::getMut(int index)
{
	//valid
	assumeArg(index >= -(int)(elements.size()) && index < (int)elements.size(), "Invalid index (%1) to access path element of '%2")
		.arg(index)
		.arg(toString())
		.end();
	
	//order
	if (index >= 0) {
		auto it = elements.begin();
		std::advance(it,index);
		return *it;
	} else {
		auto it = elements.rbegin();
		std::advance(it,-index-1);
		return *it;
	}
}

/*******************  FUNCTION  *********************/
const std::string & UnixPath::get(int index) const
{
	//valid
	assumeArg(index >= -(int)(elements.size()) && index < (int)elements.size(), "Invalid index (%1) to access path element of '%2")
		.arg(index)
		.arg(toString())
		.end();
	
	//order
	if (index >= 0) {
		auto it = elements.begin();
		std::advance(it,index);
		return *it;
	} else {
		auto it = elements.rbegin();
		std::advance(it,-index-1);
		return *it;
	}
}

/*******************  FUNCTION  *********************/
const StringList & UnixPath::getList(void) const
{
	return elements;
}

/*******************  FUNCTION  *********************/
int UnixPath::size(void) const
{
	return elements.size();
}

/*******************  FUNCTION  *********************/
std::string UnixPath::toString(void) const
{
	//buffer
	std::string buf;

	//abs
	if (absolute == false)
		buf = ".";

	//loop
	for (auto & elt : elements) {
		if (elt != "." && elt.empty() == false) {
			buf += CDEPS_PATH_SEPARATOR_STR;
			buf += elt;
		}
	}

	//ret
	return buf;
}

/*******************  FUNCTION  *********************/
bool UnixPath::isAbsolute(void) const
{
	return absolute;
}

/*******************  FUNCTION  *********************/
bool UnixPath::operator == (const UnixPath & ref) const
{
	if (absolute != ref.absolute)
		return false;
	else
		return elements == ref.elements;
}

/*******************  FUNCTION  *********************/
bool UnixPath::operator != (const UnixPath & ref) const
{
	if (absolute != ref.absolute)
		return true;
	else
		return elements != ref.elements;
}

/*******************  FUNCTION  *********************/
StringList UnixPath::split(const std::string & path)
{
	//buffer
	std::string buf;
	StringList list;

	//check
	//assumeArg(path[0] == CDEPS_PATH_SEPARATOR_CHAR, "Invalid path '%1', should start by " CDEPS_PATH_SEPARATOR_STR)
	//	.arg(path)
	//	.end();

	//loop
	for (size_t i = 0 ; i < path.size() ; ++i) {
		//if sep
		if (path[i] == CDEPS_PATH_SEPARATOR_CHAR) {
			if (buf.empty() == false && buf != ".")
				list.push_back(buf);
			buf.clear();
		} else {
			//accumuate
			buf += path[i];
		}
	}

	//flash
	if (buf.empty() == false)
		list.push_back(buf);

	return list;
}

/*******************  FUNCTION  *********************/
UnixPath & UnixPath::makeAbsolute(void)
{
	return makeAbsolute(UnixPath("."));
}

/*******************  FUNCTION  *********************/
UnixPath & UnixPath::markAbsolute(void)
{
	absolute = true;
	return *this;
}

/*******************  FUNCTION  *********************/
UnixPath & UnixPath::makeAbsolute(const UnixPath & ref)
{
	//apply prefix
	if (ref.absolute == false && absolute == false) {
		UnixPath tmp(UnixOS::getCurrentDir());
		tmp.append(ref);
		tmp.append(*this);
		*this = tmp;
	} else if (absolute == false) {
		UnixPath tmp(ref);
		tmp.append(*this);
		*this = tmp;
	}

	//return
	return *this;
}

/*******************  FUNCTION  *********************/
std::ostream & operator << (std::ostream & out, const UnixPath & path)
{
	out << path.toString();
	return out;
}

}
