/*****************************************************
			 PROJECT  : cdeps
			 VERSION  : 0.0.0-dev
			 DATE     : 11/2018
			 LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
//std
#include <cstdlib>
#include <cstring>

//unix
#include <unistd.h>
#include <libgen.h>
#include <sys/stat.h>

//current
#include "../../common/Debug.hpp"
#include "UnixOS.hpp"

/********************  NAMESPACE  *******************/
namespace cdeps
{

/*******************  FUNCTION  *********************/
std::string UnixOS::getCurrentDir(void)
{
	//get current
	char * tmp = get_current_dir_name();
	std::string dir = tmp;
	free(tmp);
	return dir;
}

/*******************  FUNCTION  *********************/
std::string UnixOS::dirname(const std::string & path)
{
	char * buf = strdup(path.c_str());
	std::string res = ::dirname(buf);
	free(buf);
	return res;
}

/*******************  FUNCTION  *********************/
bool UnixOS::fileExists(const std::string & path)
{
	struct stat buffer;   
	return (stat (path.c_str(), &buffer) == 0); 
}

/*******************  FUNCTION  *********************/
std::string UnixOS::mktemp(const std::string & format)
{
	return std::string("/tmp/")+DAQ::FormattedMessage(format).arg(rand()).toString();
}

/*******************  FUNCTION  *********************/
void UnixOS::unlink(const std::string & fname)
{
	//do
	int res = ::unlink(fname.c_str());
	assumeArg(res == 0,"Fail to remove file %1 : %2").arg(fname).argStrErrno().end();
}

/*******************  FUNCTION  *********************/
bool UnixOS::system(std::string command, bool silent)
{
	//make silent
	if (silent)
		command += " 1> /dev/null 2>/dev/null";

	//run
	int res = ::system(command.c_str());

	//ret
	return res == 0;
}

}
