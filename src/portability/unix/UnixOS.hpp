/*****************************************************
			 PROJECT  : cdeps
			 VERSION  : 0.0.0-dev
			 DATE     : 11/2018
			 LICENSE  : CeCILL-C
*****************************************************/

#ifndef CDEPS_UNIX_OS_HPP
#define CDEPS_UNIX_OS_HPP

/********************  HEADERS  *********************/
#include <string>

/********************  NAMESPACE  *******************/
namespace cdeps
{

/*********************  STRUCT  *********************/
struct UnixOS
{
	static std::string dirname(const std::string & path);
	static std::string getCurrentDir(void);
	static bool fileExists(const std::string & path);
	static std::string mktemp(const std::string & format);
	static void unlink(const std::string & fname);
	static bool system(std::string command, bool silent = false);
};

}

#endif //CDEPS_UNIX_OS_HPP
