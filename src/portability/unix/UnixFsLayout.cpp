/*****************************************************
			 PROJECT  : cdeps
			 VERSION  : 0.0.0-dev
			 DATE     : 11/2018
			 LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include "../../common/Debug.hpp"
#include "UnixFsLayout.hpp"
#include "../Path.hpp"

/********************  NAMESPACE  *******************/
namespace cdeps
{

/*******************  FUNCTION  *********************/
PathList UnixFsLayout::getAbsolute(Path path, const PathList & prefixes)
{
	//vars
	PathList out;
	const std::string & root = path.popRoot();

	//some
	const Path usr("/usr");
	const Path usrLocal("/usr/local");
	const Path slash("/");

	//special case for absolute, no prefix
	if (root == "absolute") {
		out.push_back(path.markAbsolute());
	} else {
		//for all prefix
		for (auto & sprefix : prefixes) {
			//convert
			Path prefix(sprefix);

			//check
			assumeArg(prefix.isAbsolute(), "Invalid prefix path, should be absolute, meaning start by '/', get '%1'")
				.arg(prefix)
				.end();

			//case depending on root
			if (root == "include") {
				if (prefix != slash)
					out.push_back(prefix.append("include").append(path));
			} else if (root == "lib") {
				out.push_back(prefix.clone().append("lib").append(path));
				#ifdef __x86_64__
					out.push_back(prefix.clone().append("lib/x86_64-linux-gnu").append(path));
					out.push_back(prefix.clone().append("lib64").append(path));
				#endif //__x86_64__
			} else if (root == "share") {
				if (prefix != slash)
					out.push_back(prefix.append("share").append(path));
			} else if (root == "etc") {
				if (prefix != usr && prefix != usrLocal)
					out.push_back(prefix.append("etc").append(path));
			} else if (root == "var") {
				if (prefix != usr && prefix != usrLocal)
					out.push_back(prefix.append("var").append(path));
			} else if (root == "opt") {
				if (prefix == slash)
					out.push_back(prefix.append("opt").append(path));
			} else if (root == "proc") {
				if (prefix == slash)
					out.push_back(prefix.append("proc").append(path));
			} else if (root == "sys") {
				if (prefix == slash)
					out.push_back(prefix.append("sys").append(path));
			} else {
				CDEPS_FATAL_ARG("Invalid path type with root '%1' in : '%2'")
					.arg(root)
					.arg(path)
					.end();
			}
		}
	}

	//return all
	return out;
}

/*******************  FUNCTION  *********************/
PathList UnixFsLayout::getDefaultPrefixes(void)
{
	PathList out;
	out.push_back(Path("/"));
	out.push_back(Path("/usr/"));
	out.push_back(Path("/usr/local/"));
	return out;
}

/*******************  FUNCTION  *********************/
std::string UnixFsLayout::getDynLibFilename(const std::string & libname)
{
	return std::string("lib") + libname + std::string(".so");
}

}
