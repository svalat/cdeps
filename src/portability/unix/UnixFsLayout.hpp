/*****************************************************
			 PROJECT  : cdeps
			 VERSION  : 0.0.0-dev
			 DATE     : 11/2018
			 LICENSE  : CeCILL-C
*****************************************************/

#ifndef CDEPS_UNIX_FS_LAYOUT_HPP
#define CDEPS_UNIX_FS_LAYOUT_HPP

/********************  HEADERS  *********************/
#include "../../common/Helper.hpp"
#include "../Path.hpp"

/********************  NAMESPACE  *******************/
namespace cdeps
{

/*********************  STRUCT  *********************/
struct UnixFsLayout
{
	static PathList getAbsolute(Path path, const PathList & prefixes);
	static PathList getDefaultPrefixes(void);
	static std::string getDynLibFilename(const std::string & libname);
};

}

#endif //CDEPS_UNIX_FS_LAYOUT_HPP
