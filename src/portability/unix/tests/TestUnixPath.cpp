/*****************************************************
			 PROJECT  : cdeps
			 VERSION  : 0.0.0-dev
			 DATE     : 11/2018
			 LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
//unix
#include <unistd.h>

//google
#include <gtest/gtest.h>

//local
#include "../UnixPath.hpp"

/***************** USING NAMESPACE ******************/
using namespace cdeps;

/*******************  FUNCTION  *********************/
TEST(TestUnixPath,constructor_1)
{
	UnixPath p1("/usr/src/linux/Makefile");

	EXPECT_EQ(p1.size(), 4);
	EXPECT_EQ(p1.get(0),"usr");
	EXPECT_EQ(p1.get(1),"src");
	EXPECT_EQ(p1.get(2),"linux");
	EXPECT_EQ(p1.get(3),"Makefile");
}

/*******************  FUNCTION  *********************/
TEST(TestUnixPath,constructor_2)
{
	UnixPath p1("/usr/src/linux/Makefile");

	EXPECT_EQ(p1.size(), 4);
	EXPECT_EQ(p1.get(0),"usr");
	EXPECT_EQ(p1.get(1),"src");
	EXPECT_EQ(p1.get(2),"linux");
	EXPECT_EQ(p1.get(3),"Makefile");
}

/*******************  FUNCTION  *********************/
TEST(TestUnixPath,constructor_3)
{
	UnixPath p1("//usr//src/././linux/Makefile");

	EXPECT_EQ(p1.size(), 4);
	EXPECT_EQ(p1.get(0),"usr");
	EXPECT_EQ(p1.get(1),"src");
	EXPECT_EQ(p1.get(2),"linux");
	EXPECT_EQ(p1.get(3),"Makefile");
}

/*******************  FUNCTION  *********************/
TEST(TestUnixPath,append_string)
{
	UnixPath p1("/usr");
	
	p1.append("Makefile");

	EXPECT_EQ(p1.size(), 2);
	EXPECT_EQ(p1.get(0),"usr");
	EXPECT_EQ(p1.get(1),"Makefile");
}

/*******************  FUNCTION  *********************/
TEST(TestUnixPath,append_string_many)
{
	UnixPath p1("/usr");

	p1.append("/bob/Makefile");

	EXPECT_EQ(p1.size(), 3);
	EXPECT_EQ(p1.get(0),"usr");
	EXPECT_EQ(p1.get(1),"bob");
	EXPECT_EQ(p1.get(2),"Makefile");
}

/*******************  FUNCTION  *********************/
TEST(TestUnixPath,append_path)
{
	UnixPath p1("/usr");
	UnixPath p2("/bob/Makefile");
	
	p1.append(p2);

	EXPECT_EQ(p1.size(), 3);
	EXPECT_EQ(p1.get(0),"usr");
	EXPECT_EQ(p1.get(1),"bob");
	EXPECT_EQ(p1.get(2),"Makefile");
}

/*******************  FUNCTION  *********************/
TEST(TestUnixPath,pop)
{
	UnixPath p1("/usr/bob/Makefile");
	
	EXPECT_EQ(p1.pop(),"Makefile");
	EXPECT_EQ(p1.size(), 2);

	EXPECT_EQ(p1.pop(),"bob");
	EXPECT_EQ(p1.size(), 1);

	EXPECT_EQ(p1.pop(),"usr");
	EXPECT_EQ(p1.size(), 0);
}

/*******************  FUNCTION  *********************/
TEST(TestUnixPath,popRoot)
{
	UnixPath p1("/usr/bob/Makefile");
	EXPECT_TRUE(p1.isAbsolute());
	
	EXPECT_EQ(p1.popRoot(),"usr");
	EXPECT_EQ(p1.size(), 2);
	EXPECT_FALSE(p1.isAbsolute());

	EXPECT_EQ(p1.popRoot(),"bob");
	EXPECT_EQ(p1.size(), 1);

	EXPECT_EQ(p1.popRoot(),"Makefile");
	EXPECT_EQ(p1.size(), 0);
}

/*******************  FUNCTION  *********************/
TEST(TestUnixPath,get)
{
	UnixPath p1("/usr/bob/Makefile");
	
	EXPECT_EQ(p1.get(0),"usr");
	EXPECT_EQ(p1.get(1),"bob");

	EXPECT_EQ(p1.get(-1),"Makefile");
	EXPECT_EQ(p1.get(-2),"bob");
}

/*******************  FUNCTION  *********************/
TEST(TestUnixPath,get_abs)
{
	UnixPath p1("/usr//tmp/./ok");
	EXPECT_EQ(p1.toString(),"/usr/tmp/ok");

	UnixPath p2("usr//tmp/./ok");
	EXPECT_EQ(p2.toString(),"./usr/tmp/ok");
}

/*******************  FUNCTION  *********************/
TEST(TestUnixPath,isAbsolute)
{
	UnixPath p1("/usr//tmp/./ok");
	EXPECT_TRUE(p1.isAbsolute());

	UnixPath p2("usr//tmp/./ok");
	EXPECT_FALSE(p2.isAbsolute());
}

/*******************  FUNCTION  *********************/
TEST(TestUnixPath,operatorEqual)
{
	EXPECT_FALSE(UnixPath("/") == UnixPath("/usr"));
	EXPECT_FALSE(UnixPath("usr") == UnixPath("/usr"));
	EXPECT_TRUE(UnixPath("/usr/") == UnixPath("/usr"));
	EXPECT_TRUE(UnixPath("/usr//local") == UnixPath("/usr/local"));
}

/*******************  FUNCTION  *********************/
TEST(TestUnixPath,operatorNotEqual)
{
	EXPECT_TRUE(UnixPath("/") != UnixPath("/usr"));
	EXPECT_TRUE(UnixPath("usr") != UnixPath("/usr"));
	EXPECT_FALSE(UnixPath("/usr/") != UnixPath("/usr"));
	EXPECT_FALSE(UnixPath("/usr//local") != UnixPath("/usr/local"));
}

/*******************  FUNCTION  *********************/
TEST(TestUnixPath,makeAbsolute)
{
	ASSERT_EQ(0,chdir("/usr/local"));
	EXPECT_EQ(UnixPath("./test.txt").makeAbsolute().toString(),"/usr/local/test.txt");
	EXPECT_EQ(UnixPath("test.txt").makeAbsolute().toString(),"/usr/local/test.txt");
	EXPECT_EQ(UnixPath("/test.txt").makeAbsolute().toString(),"/test.txt");
}

/*******************  FUNCTION  *********************/
TEST(TestUnixPaths,makeRelAbsFromRef)
{
	UnixPath ref("/usr/local");
	EXPECT_EQ(UnixPath("/home/USER/test.txt").makeAbsolute(ref).toString(),"/home/USER/test.txt");
	EXPECT_EQ(UnixPath("test.txt").makeAbsolute(ref).toString(),"/usr/local/test.txt");
	EXPECT_EQ(UnixPath("./test.txt").makeAbsolute(ref).toString(),"/usr/local/test.txt");
	EXPECT_EQ(UnixPath("tmp/test.txt").makeAbsolute(ref).toString(),"/usr/local/tmp/test.txt");
}
