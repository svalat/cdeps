/*****************************************************
			 PROJECT  : cdeps
			 VERSION  : 0.0.0-dev
			 DATE     : 11/2018
			 LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
//google
#include <gtest/gtest.h>

//local
#include "../UnixFsLayout.hpp"
#include "../UnixPath.hpp"

/********************  NAMESPACE  *******************/
using namespace cdeps;

/*******************  FUNCTION  *********************/
TEST(TestUnixFsLayout,getAbsolute_absolute)
{
	Path sub("/absolute/usr/src//linux/include/mem.h");
	PathList prefixes = UnixFsLayout::getDefaultPrefixes();

	PathList out = UnixFsLayout::getAbsolute(sub,prefixes);
	EXPECT_EQ(out.size(),1);

	const char * ref[] = {"/usr/src/linux/include/mem.h","END"};

	int i = 0;
	for (auto & it : out) {
		EXPECT_EQ(it.toString(),ref[i++]);
	}
}

/*******************  FUNCTION  *********************/
TEST(TestUnixFsLayout,getAbsolute_include)
{
	Path sub("/include/cdeps/mem.h");
	PathList prefixes = UnixFsLayout::getDefaultPrefixes();

	PathList out = UnixFsLayout::getAbsolute(sub,prefixes);
	EXPECT_EQ(out.size(),2);

	const char * ref[] = {
		"/usr/include/cdeps/mem.h",
		"/usr/local/include/cdeps/mem.h",
		"END"
	};

	int i = 0;
	for (auto & it : out) {
		EXPECT_EQ(it.toString(),ref[i++]);
	}
}


/*******************  FUNCTION  *********************/
TEST(TestUnixFsLayout,getAbsolute_lib)
{
	Path sub("/lib/python/libc.so");
	PathList prefixes = UnixFsLayout::getDefaultPrefixes();

	PathList out = UnixFsLayout::getAbsolute(sub,prefixes);
	#ifdef __x86_64__
		EXPECT_EQ(out.size(),6);
	#else
		EXPECT_EQ(out.size(),3);
	#endif

	const char * ref[] = {
		"/lib/python/libc.so",
		#ifdef __x86_64__
			"/lib64/python/libc.so",
		#endif
		"/usr/lib/python/libc.so",
		#ifdef __x86_64__
			"/usr/lib64/python/libc.so",
		#endif
		"/usr/local/lib/python/libc.so",
		#ifdef __x86_64__
			"/usr/local/lib64/python/libc.so",
		#endif
		"END"
	};

	int i = 0;
	for (auto & it : out) {
		EXPECT_EQ(it.toString(),ref[i++]);
	}
}

/*******************  FUNCTION  *********************/
TEST(TestUnixFsLayout,getAbsolute_share)
{
	Path sub("/share/cmake/Modules/FindCdeps.cmake");
	PathList prefixes = UnixFsLayout::getDefaultPrefixes();

	PathList out = UnixFsLayout::getAbsolute(sub,prefixes);
	EXPECT_EQ(out.size(),2);

	const char * ref[] = {
		"/usr/share/cmake/Modules/FindCdeps.cmake",
		"/usr/local/share/cmake/Modules/FindCdeps.cmake",
		"END"
	};

	int i = 0;
	for (auto & it : out) {
		EXPECT_EQ(it.toString(),ref[i++]);
	}
}

/*******************  FUNCTION  *********************/
TEST(TestUnixFsLayout,getAbsolute_etc)
{
	Path sub("/etc/passwd");
	PathList prefixes = UnixFsLayout::getDefaultPrefixes();
	prefixes.push_back(Path("/home/ME/usr"));

	PathList out = UnixFsLayout::getAbsolute(sub,prefixes);
	EXPECT_EQ(out.size(),2);

	const char * ref[] = {
		"/etc/passwd",
		"/home/ME/usr/etc/passwd",
		"END"
	};

	int i = 0;
	for (auto & it : out) {
		EXPECT_EQ(it.toString(),ref[i++]);
	}
}

/*******************  FUNCTION  *********************/
TEST(TestUnixFsLayout,getAbsolute_var)
{
	Path sub("/var/spool/test.pid");
	PathList prefixes = UnixFsLayout::getDefaultPrefixes();
	prefixes.push_back(Path("/home/ME/usr"));

	PathList out = UnixFsLayout::getAbsolute(sub,prefixes);
	EXPECT_EQ(out.size(),2);

	const char * ref[] = {
		"/var/spool/test.pid",
		"/home/ME/usr/var/spool/test.pid",
		"END"
	};

	int i = 0;
	for (auto & it : out) {
		EXPECT_EQ(it.toString(),ref[i++]);
	}
}

/*******************  FUNCTION  *********************/
TEST(TestUnixFsLayout,getAbsolute_invalid)
{
	Path sub("/unknown/spool/test.pid");
	PathList prefixes = UnixFsLayout::getDefaultPrefixes();

	EXPECT_EXIT(UnixFsLayout::getAbsolute(sub,prefixes), ::testing::ExitedWithCode(1), "Invalid path");
}

/*******************  FUNCTION  *********************/
TEST(TestUnixFsLayout,getDynLibFilename)
{
	EXPECT_EQ(UnixFsLayout::getDynLibFilename("pthread"),"libpthread.so");
}
