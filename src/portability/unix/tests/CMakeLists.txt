######################################################
#            PROJECT  : cdeps                        #
#            VERSION  : 0.0.0-dev                    #
#            DATE     : 10/2018                      #
#            LICENSE  : CeCILL-C                     #
######################################################

######################################################
#Use gtest
include_directories(${GTEST_INCLUDE_DIRS} ${GMOCK_INCLUDE_DIRS})
include_directories(../)

######################################################
add_definitions(-DDATADIR="${CMAKE_CURRENT_SOURCE_DIR}")

######################################################
set(TEST_NAMES TestUnixPath TestUnixOS TestUnixFsLayout)

######################################################
FOREACH(test_name ${TEST_NAMES})
	add_executable(${test_name} ${test_name}.cpp)
	target_link_libraries(${test_name} ${GTEST_BOTH_LIBRARIES} ${GMOCK_BOTH_LIBRARIES} cdeps_internal)
	cdeps_add_test(${test_name} ${test_name})
ENDFOREACH(test_name)
