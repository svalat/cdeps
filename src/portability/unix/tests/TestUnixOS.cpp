/*****************************************************
			 PROJECT  : cdeps
			 VERSION  : 0.0.0-dev
			 DATE     : 11/2018
			 LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
//unix
#include <unistd.h>

//google
#include <gtest/gtest.h>

//local
#include "../UnixOS.hpp"

/***************** USING NAMESPACE ******************/
using namespace cdeps;

/*******************  FUNCTION  *********************/
TEST(TestUnixOS,dirname)
{
	EXPECT_EQ(UnixOS::dirname("/usr/local"),"/usr");
	EXPECT_EQ(UnixOS::dirname("/usr"),"/");
	EXPECT_EQ(UnixOS::dirname("./"),".");
}

/*******************  FUNCTION  *********************/
TEST(TestUnixOS,fileExists)
{
	EXPECT_TRUE(UnixOS::fileExists(DATADIR "/CMakeLists.txt"));
	EXPECT_FALSE(UnixOS::fileExists(DATADIR "/CMakeLists.txt-NotThere"));
}

/*******************  FUNCTION  *********************/
TEST(TestUnixOS,mktemp)
{
	std::string name = UnixOS::mktemp("cdeps-test-mktemp-%1.txt");
	ASSERT_FALSE(name.empty());
}

/*******************  FUNCTION  *********************/
TEST(TestUnixOS, system)
{
	std::string name = UnixOS::mktemp("cdeps-test-system-%1.txt");
	ASSERT_FALSE(UnixOS::fileExists(name));
	
	UnixOS::system(std::string("touch ")+name);

	ASSERT_TRUE(UnixOS::fileExists(name));
	UnixOS::unlink(name);
}
