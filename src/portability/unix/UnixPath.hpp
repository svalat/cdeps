/*****************************************************
			 PROJECT  : cdeps
			 VERSION  : 0.0.0-dev
			 DATE     : 11/2018
			 LICENSE  : CeCILL-C
*****************************************************/

#ifndef CDEPS_UNIX_PATHS_HPP
#define CDEPS_UNIX_PATHS_HPP

/********************  HEADERS  *********************/
#include <string>
#include "../../common/Helper.hpp"

/********************  NAMESPACE  *******************/
namespace cdeps
{

/********************  MACROS  **********************/
#define CDEPS_PATH_SEPARATOR_CHAR '/'
#define CDEPS_PATH_SEPARATOR_STR "/"

/*********************  CLASS  **********************/
class UnixPath
{
	public:
		UnixPath(const std::string path = ".");
		UnixPath & append(const std::string & subPath);
		UnixPath & append(const UnixPath & subPath);
		UnixPath & makeAbsolute(void);
		UnixPath & makeAbsolute(const UnixPath & ref);
		std::string pop(void);
		std::string popRoot(void);
		std::string & getMutable(int index);
		std::string & getMut(int index);
		const std::string & get(int index) const;
		const StringList & getList(void) const;
		int size(void) const;
		std::string toString(void) const;
		bool isAbsolute(void) const;
		UnixPath & markAbsolute(void);
		UnixPath clone(void);
		bool operator == (const UnixPath & ref) const;
		bool operator != (const UnixPath & ref) const;
	private:
		static StringList split(const std::string & path);
	private:
		StringList elements;
		bool absolute;
};

/*********************  TYPES  **********************/
typedef std::list<UnixPath> UnixPathList;

/*******************  FUNCTION  *********************/
std::ostream & operator << (std::ostream & out, const UnixPath & path);

}

#endif //CDEPS_UNIX_PATHS_HPP
