/*****************************************************
			 PROJECT  : cdeps
			 VERSION  : 0.0.0-dev
			 DATE     : 11/2018
			 LICENSE  : CeCILL-C
*****************************************************/

#ifndef CDEPS_OS_HPP
#define CDEPS_OS_HPP

/********************  HEADERS  *********************/
#include "config.h"

/********************  HEADERS  *********************/
#ifdef PORTABILITY_OS_UNIX
	#include "unix/UnixOS.hpp"
	namespace cdeps
	{
		typedef UnixOS OS;
	}
#else
	#error Unsupported portability mode for OS, should be UNIX
#endif

#endif //CDEPS_OS_HPP
