/*****************************************************
			 PROJECT  : cdeps
			 VERSION  : 0.0.0-dev
			 DATE     : 11/2018
			 LICENSE  : CeCILL-C
*****************************************************/

#ifndef CDEPS_PATHS_HPP
#define CDEPS_PATHS_HPP

/********************  HEADERS  *********************/
#include "config.h"
#include <list>

/********************  HEADERS  *********************/
#ifdef PORTABILITY_OS_UNIX
	#include "unix/UnixPath.hpp"
	namespace cdeps
	{
		typedef UnixPath Path;
		typedef UnixPathList PathList;
	}
#else
	#error Unsupported portability mode for OS, should be UNIX
#endif

#endif //CDEPS_PATHS_HPP
