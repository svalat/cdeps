######################################################
#            PROJECT  : cdeps                        #
#            VERSION  : 0.0.0-dev                    #
#            DATE     : 10/2018                      #
#            LICENSE  : CeCILL-C                     #
######################################################

######################################################
#vars
set(ENABLE_COLOR ON CACHE BOOL "Enable colors by default in cdeps output")
set(WITH_OS "Unix" CACHE STRING "On which OS we are running on to adapt system functions and rules. Currently only support 'Unix")

######################################################
#default
set(PORTABILITY_OS_UNIX OFF)
#portabilty twicks
if (WITH_OS STREQUAL "Unix")
	set(PORTABILITY_OS_UNIX ON)
endif()

######################################################
#setup config file
configure_file(config.h.in "${CMAKE_CURRENT_BINARY_DIR}/config.h" @ONLY)
include_directories(${CMAKE_CURRENT_BINARY_DIR})

######################################################
include_directories(${CMAKE_SOURCE_DIR}/extern-deps)
include_directories(${CMAKE_SOURCE_DIR}/extern-deps/tclap-1.2.2/include)

######################################################
set(CDEPS_INTERNAL_CODE
	#extern deps
	$<TARGET_OBJECTS:daqpipe-common>
	$<TARGET_OBJECTS:homelinux-core>
	#internal
	$<TARGET_OBJECTS:cdeps-common>
	$<TARGET_OBJECTS:cdeps-core>
	$<TARGET_OBJECTS:cdeps-runner>
	#we should keep an eye on portability of this
	$<TARGET_OBJECTS:cdeps-portability>
	$<TARGET_OBJECTS:homelinux-portability>
)

######################################################
add_library(cdeps_internal SHARED ${CDEPS_INTERNAL_CODE})

######################################################
add_executable(cdeps ${CDEPS_INTERNAL_CODE} main.cpp)

######################################################
add_subdirectory(core)
add_subdirectory(common)
add_subdirectory(portability)
add_subdirectory(runner)
