/*****************************************************
			 PROJECT  : cdeps
			 VERSION  : 0.0.0-dev
			 DATE     : 10/2018
			 LICENSE  : CeCILL-C
*****************************************************/

#ifndef CDEPS_SPEC_FILE_HPP
#define CDEPS_SPEC_FILE_HPP

/********************  HEADERS  *********************/
//std
#include <string>
#include <list>
#include <ostream>

//internal
#include "../common/Helper.hpp"

/********************  NAMESPACE  *******************/
namespace cdeps
{

/*********************  ENUM  ***********************/
enum SpecLineType
{
	SPEC_LINE_EMPTY,
	SPEC_LINE_COMMENT,
	SPEC_LINE_COMMAND,
	SPEC_LINE_PATH,
	SPEC_LINE_NAME,
};

/*********************  STRUCT  *********************/
struct SourceLine
{
	std::string filename;
	int line;
};

/*********************  STRUCT  *********************/
struct SpecFileLine
{
	//orig
	SourceLine source;
	//parsed infos
	int level;
	SpecLineType type;
	std::string name;
	std::string args;
	std::string full;
	bool expectChild;
	//functions
	void requireArgs(void) const;
	void doAssume(bool value, const std::string & msg) const;
	void doFatal(std::string msg) const;
};

/*********************  TYPES  **********************/
struct SpecFileNode;
typedef std::list<SpecFileNode> SpecFileNodeChildList;
typedef SpecFileNodeChildList SpecTreeRoot;

/********************  STRUCT  **********************/
struct SpecFileNode
{
	//definition
	SpecFileLine def;
	//childs
	SpecFileNodeChildList childs;
};

/*********************  TYPES  **********************/
typedef std::list<SpecFileLine> SpecFileLineList;

/*********************  CLASS  **********************/
class SpecFile
{
	public:
		SpecFile(const std::string & filename = "");
		~SpecFile(void);
		void loadFile(const std::string & filename);
		void loadContent(const FileLines & content, const std::string & filename);
		void dump(FileLines & out);
		const SpecFileLineList & getEntries(void) const;
		void extractTree(SpecTreeRoot & out) const;
	public:
		void applyIncludes(void);
		static SpecFileLineList::const_iterator internalExtractSubTree(SpecFileNodeChildList & out, SpecFileLineList::const_iterator itStart, SpecFileLineList::const_iterator itEnd);
		static std::string getIndentRef(const FileLines & content, const std::string & filename);
		static std::string getIndentString(const std::string & line);
		static int getIndentLevel(const std::string & indent,const std::string & ref);
		static void parseLine(SpecFileLine & out, const std::string & line, const std::string & indentRef);
		static void parseLines(SpecFileLineList & out, const FileLines & content, const std::string & filename);
		static void sanityCheckLevelHierarchy(SpecFileLineList & entries);
	private:
		std::string filename;
		SpecFileLineList entries;
};

/*******************  FUNCTION  *********************/
std::ostream & operator << (std::ostream & out, const SourceLine & source);
std::ostream & operator << (std::ostream & out, const SpecFileLine & line);

}

#endif //CDEPC_SPEC_FILE_HPP
