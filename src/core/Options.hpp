/*****************************************************
			 PROJECT  : cdeps
			 VERSION  : 0.0.0-dev
			 DATE     : 11/2018
			 LICENSE  : CeCILL-C
*****************************************************/

#ifndef CDEPS_OPTIONS_HPP
#define CDEPS_OPTIONS_HPP

/********************  HEADERS  *********************/
#include "../common/Helper.hpp"

/********************  NAMESPACE  *******************/
namespace cdeps
{

/*********************  TYPES  **********************/
//CAUTION, don't forget to update cstGenerator in cpp file
enum OptionGenerator
{
	CDEPS_GENERATOR_TEXT,
	//CDEPS_GENERATOR_JSON,
	//CDEPS_GENERATOR_XML,
	//CDEPS_GENERATOR_SHELL,
	//CDEPS_GENERATOR_MAKEFILE,
	//CDEPS_GENERATOR_CMAKE,
	//CDEPS_GENERATOR_AUTOTOOLS,
	CDEPS_GENERATOR_COUNT,
};

/*********************  CLASS  **********************/
class Options
{
	public:
		Options(void);
		void loadEnv(void);
		bool parseArgs(int argc, char ** argv);
	private:
		void loadAutoSysDefines(void);
		static OptionGenerator stringToGenerator(const std::string & value);
	public:
		StringList defines;
		StringList prefixes;
		std::string filename;
		OptionGenerator generator;
};

};

#endif //CDEPDS_OPTIONS_HPP
