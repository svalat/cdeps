/*****************************************************
			 PROJECT  : cdeps
			 VERSION  : 0.0.0-dev
			 DATE     : 11/2018
			 LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
//std
#include <vector>

//extern
#include <tclap/CmdLine.h>

//local
#include "config.h"
#include "../common/Debug.hpp"
#include "Options.hpp"

/********************  NAMESPACE  *******************/
namespace cdeps
{

/********************  CONSTS  **********************/
const char * cstBrief = "Cdeps is a dependency manager to help handling deps in project. \
It can self search and generate Makefile output or generate directly cmake/autotools \
ackage search files.";
const char * cstGenerator[CDEPS_GENERATOR_COUNT] = {
	"text",
	/*"json",
	"xml",
	"shell",
	"makefile",
	"cmake",
	"autotools"*/
};

/*******************  FUNCTION  *********************/
Options::Options(void)
{
	this->filename = "dependencies.cdeps";
	this->generator = CDEPS_GENERATOR_TEXT;
}

/*******************  FUNCTION  *********************/
OptionGenerator Options::stringToGenerator(const std::string & value)
{
	//loop
	for (int i = 0 ; i < CDEPS_GENERATOR_COUNT ; i++)
		if (value == cstGenerator[i])
			return static_cast<OptionGenerator>(i);
	
	//failure
	CDEPS_FATAL_ARG("Invalid generator type : '%1'").arg(value).end();
	return CDEPS_GENERATOR_TEXT;
}

/*******************  FUNCTION  *********************/
void Options::loadEnv(void)
{
	//prefixes
	char * envPrefixes = getenv("CDEPS_PREFIXES");
	if (envPrefixes != NULL) {
		this->prefixes.splice(this->prefixes.end(),Helper::split(envPrefixes,':',false));
	}
}

/*******************  FUNCTION  *********************/
void Options::loadAutoSysDefines(void)
{
	//arch
	#ifdef __x86_64__
		this->defines.push_back("ARCH_X86_64");
	#endif
}

/*******************  FUNCTION  *********************/
bool Options::parseArgs(int argc, char ** argv)
{
	try {
		//main
		TCLAP::CmdLine cmd(cstBrief, ' ', CDEPS_VERSION);

		//filename
		TCLAP::ValueArg<std::string> argFilename("f","file","Cdeps file to load.",false,this->filename,"FILE");
		cmd.add(argFilename);

		//generator
		std::vector<std::string> allowedGeneratorStrings;
		for (int i = 0 ; i < CDEPS_GENERATOR_COUNT ; i++)
			allowedGeneratorStrings.push_back(cstGenerator[i]);
		TCLAP::ValuesConstraint<std::string> allowedGenerator( allowedGeneratorStrings );
		TCLAP::ValueArg<std::string> argGenerator("G","generator","What to generate as output.",false,"MODE",&allowedGenerator,cmd);

		//defines
		TCLAP::MultiArg<std::string> argDefines("D", "define", "Define a value to enable some features", false,"VARIABLE");
		cmd.add(argDefines);

		//prefixes
		TCLAP::MultiArg<std::string> argPrefixes("p", "prefix", "Define a prefix to add to search list", false,"PATH");
		cmd.add(argPrefixes);

		//parse
		cmd.parse(argc,argv);

		//extract
		this->filename = argFilename.getValue();
		this->generator = stringToGenerator(argGenerator.getValue());
		this->defines.splice(this->defines.end(),Helper::vectorToList(argDefines.getValue()));
		this->prefixes.splice(this->prefixes.end(),Helper::vectorToList(argPrefixes.getValue()));

		//load
		this->loadAutoSysDefines();

		//ok
		return true;
	} catch (TCLAP::ArgException &e) {
		CDEPS_ERROR_ARG("%1 for arg %2").arg(e.error()).arg(e.argId()).end();
		return false;
	}
}

}
