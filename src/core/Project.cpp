/*****************************************************
			 PROJECT  : cdeps
			 VERSION  : 0.0.0-dev
			 DATE     : 11/2018
			 LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
//std
#include <cassert>

//hl
#include "from-homelinux-v2.0.0/VersionMatcher.hpp"

//local
#include "config.h"
#include "../common/Debug.hpp"
#include "Project.hpp"

/********************  NAMESPACE  *******************/
namespace cdeps
{

/*******************  FUNCTION  *********************/
Project::Project(const Options * options)
{
	this->options = options;
}

/*******************  FUNCTION  *********************/
Project::~Project(void)
{
}

/*******************  FUNCTION  *********************/
void Project::loadSpec(const SpecFile & spec)
{
	//extract tree
	SpecTreeRoot root;
	spec.extractTree(root);

	//loop on all
	for (auto & node : root) {
		//root commands
		if (node.def.type == SPEC_LINE_COMMAND) {
			parseRootCommands(node);
		} else if (node.def.type == SPEC_LINE_NAME) {
			this->packages.emplace_back();
			this->packages.back().loadFromTree(node);
		} else {
			CDEPS_FATAL_ARG("Invalid command in project file (%1) at %2")
				.arg(node.def)
				.arg(node.def.source)
				.end();
		}
	}

	//apply if statements
	this->applyIfStatements();
}

/*******************  FUNCTION  *********************/
void Project::parseRootCommands(const SpecFileNode & cmd)
{
	//check
	assert(cmd.def.type == SPEC_LINE_COMMAND);
	assert(cmd.def.level == 0);

	//manage
	if (cmd.def.name == "cdeps_minimum_required") {
		rootCommandCheckMinCdepsReq(cmd);
	} else if (cmd.def.name == "plugins") {
		rootCommandPlugins(cmd);
	} else {
		CDEPS_FATAL_ARG("Invalid command in root of project file (%1) at %2")
			.arg(cmd.def)
			.arg(cmd.def.source)
			.end();
	}
}

/*******************  FUNCTION  *********************/
void Project::rootCommandCheckMinCdepsReq(const SpecFileNode & cmd)
{
	//check sanity
	assume(cmd.def.expectChild == false,"Command [cdeps_minimum_required] cannot have childs !");
	
	//build operator
	std::string op = std::string(">=")+cmd.def.args;
	
	//check version matching
	assumeArg(hl::VersionMatcher::applyVersionOperator(op,getCleanVersion()),"Current CDEPS (%1) version is too old for this project, required is minimum %2")
		.arg(CDEPS_VERSION)
		.arg(cmd.def.args)
		.end();
}

/*******************  FUNCTION  *********************/
void Project::rootCommandPlugins(const SpecFileNode & cmd)
{
	//loop on childs
	for (auto & entry : cmd.childs) {
		if (entry.def.type == SPEC_LINE_NAME) {
			if (entry.def.name == "core") {
				//nothing
			} else {
				CDEPS_FATAL_ARG("Unknown plugin '%1' loaded in [plugins] at %2").arg(entry.def.name).arg(entry.def.source).end();
			}
		} else {
			CDEPS_FATAL_ARG("Invalid child node '%1' inside [plugins] at %2").arg(entry.def).arg(entry.def.source).end();
		}
	}
}

/*******************  FUNCTION  *********************/
std::string Project::getCleanVersion(void)
{
	std::string v = CDEPS_VERSION;
	size_t pos = v.find_first_of('-');
	if (pos == std::string::npos)
		return v;
	else
		return v.substr(0,pos);
}

/*******************  FUNCTION  *********************/
PackageList & Project::getPackageList(void)
{
	return packages;
}

/*******************  FUNCTION  *********************/
const PackageList & Project::getPackageList(void) const
{
	return packages;
}

/*******************  FUNCTION  *********************/
PathList Project::getPrefixes(void) const
{
	//vars
	PathList paths;

	//convert
	for (auto & it : this->options->prefixes) {
		paths.push_back(Path(it));
	}

	return paths;
}

/*******************  FUNCTION  *********************/
void Project::applyIfStatements(void)
{
	for (auto & pack : packages) {
		if (defineMakesOptional(pack.ifHasVar))
			pack.optional = true;
		for (auto & file : pack.files) {
			if (defineMakesOptional(file.ifHasVar))
				file.optional = true;
			for (auto & sym : file.symbols) {
				if (defineMakesOptional(sym.ifHasVar))
					sym.optional = true;
			}
		}
	}
}

/*******************  FUNCTION  *********************/
bool Project::defineMakesOptional(const StringList & vars) const
{
	//trivial
	if (vars.empty())
		return false;

	//check all
	for (auto & vname : vars) {
		for (auto & it : this->options->defines)
			if (it == vname)
				return false;
	}

	//no define makes optional
	return true;
}

/*******************  FUNCTION  *********************/
void Project::setVariable(const std::string & name, const std::string & value, bool append)
{
	if (this->variables.find(name) == this->variables.end())
		this->variables[name] = value;
	else
		this->variables[name] += std::string(":") + value;
}

/*******************  FUNCTION  *********************/
const VariableMap & Project::getVariables(void) const
{
	return this->variables;
}

}
