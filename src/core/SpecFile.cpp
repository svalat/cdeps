/*****************************************************
			 PROJECT  : cdeps
			 VERSION  : 0.0.0-dev
			 DATE     : 10/2018
			 LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
//std
#include <cassert>
#include <algorithm>
#include <sstream>
#include <iterator>

//project
#include "SpecFile.hpp"
#include "../common/Debug.hpp"
#include "../portability/Path.hpp"
#include "../portability/OS.hpp"

/***************** USING NAMESPACE ******************/
using namespace std;

/********************  NAMESPACE  *******************/
namespace cdeps
{

/*******************  FUNCTION  *********************/
SpecFile::SpecFile(const std::string & filename)
{
	if (filename.empty() == false)
		this->loadFile(filename);
}

/*******************  FUNCTION  *********************/
SpecFile::~SpecFile(void)
{

}

/*******************  FUNCTION  *********************/
void SpecFile::loadFile(const std::string & filename)
{
	//load file
	FileLines content;
	Helper::loadFileLines(content,filename);
	this->filename = filename;

	//load
	this->loadContent(content,filename);
}

/*******************  FUNCTION  *********************/
void SpecFile::loadContent(const FileLines & content, const std::string & filename)
{
	//parse all entries
	this->parseLines(this->entries,content,this->filename);

	//check level hierarchy
	this->sanityCheckLevelHierarchy(this->entries);

	//apply includes
	this->applyIncludes();
}

/*******************  FUNCTION  *********************/
std::string SpecFile::getIndentString(const std::string & line)
{
	//loop on all white spaces and tabs
	int cur = 0;
	for (size_t i = 0 ; i < line.size() ; i++) {
		if (line[i] == ' ' || line[i] == '\t')
			cur++;
		else
			break;
	}

	//extract indent
	return line.substr(0,cur);
}

/*******************  FUNCTION  *********************/
int SpecFile::getIndentLevel(const std::string & indent,const std::string & ref)
{
	//really trivial
	if (indent.empty())
		return 0;

	//trivial case, non multiple
	if (indent.size() % ref.size() != 0)
		return -1;

	//loop on all
	int level = 0;
	while (level*ref.size() < indent.size()) {
		if (indent.substr(level*ref.size(),ref.size()) != ref)
			return -1;
		else
			level++;
	}

	//ok we get it
	return level;
}

/*******************  FUNCTION  *********************/
std::string SpecFile::getIndentRef(const FileLines & content,const std::string & filename)
{
	//vars
	std::string indentRef = "";

	//loop each line
	int lineId = 1;
	for (auto line : content) {
		//get indent of line
		std::string cur = getIndentString(line);

		//if not yet have first indent
		if (indentRef.empty() && cur.empty() == false)
			indentRef = cur;

		//get indent level
		int level = getIndentLevel(cur,indentRef);

		//check
		assumeArg(level >= 0,"Invalid indentation on line %1 of '%2', non fixed multiple spaces of tab/space mix")
			.arg(lineId)
			.arg(filename)
			.end();

		lineId++;
	}

	//ok we get it
	return indentRef;
}

/*******************  FUNCTION  *********************/
void SpecFile::parseLine(SpecFileLine & out, const std::string & line, const std::string & indentRef)
{
	//full
	out.full = line;

	//extract indent
	const std::string indent = getIndentString(line);
	const int indentLevel = getIndentLevel(indent,indentRef);
	out.level = indentLevel;
	assert(indentLevel >= 0);

	//remove indent from string
	const std::string cleanLine = line.substr(indent.size(),line.size() - indent.size());

	//extract
	int start = 0;
	int end = cleanLine.size();

	//get last
	char last = cleanLine[end-1];
	//@TODO remove end indent chars
	assert(last != ' ');
	assert(last != '\t');

	//child
	if (last == ':') {
		out.expectChild = true;
		end--;
		last = cleanLine[end - 1];
	} else {
		out.expectChild = false;
	}

	//detect type
	if (cleanLine[0] == '#') {
		out.type = SPEC_LINE_COMMENT;
		start = 1;
	} else if (cleanLine[0] == '/') {
		out.type = SPEC_LINE_PATH;
	} else if (cleanLine[0] == '[') {
		out.type = SPEC_LINE_COMMAND;
		assumeArg(last == ']', "Mismatching command open/close symbols, expect ] at then end of the line on %1 : %2")
			.arg(out.source)
			.arg(cleanLine)
			.end();
		start++;
		end--;
	} else if (start == end) {
		out.type = SPEC_LINE_EMPTY;
	} else {
		out.type = SPEC_LINE_NAME;
	}

	//extract
	if (out.type == SPEC_LINE_COMMAND) {
		//search indent base
		int cut = 0;
		while (cut < end && cleanLine[cut] != ' ' && cleanLine[cut] != '\t')
			cut++;
		//get name
		out.name = cleanLine.substr(start,cut - start);
		//trim args
		while (cut < end && (cleanLine[cut] == ' ' || cleanLine[cut] == '\t'))
			cut++;
		out.args = cleanLine.substr(cut,end - cut);
	} else {
		out.name = cleanLine.substr(start,end - start);
		out.args.clear();
	}
}

/*******************  FUNCTION  *********************/
void SpecFile::parseLines(SpecFileLineList & out, const FileLines & content, const std::string & filename)
{
	//extract ident style
	std::string indentRef = getIndentRef(content,filename);

	//loop on lines and extract infos
	int lineId = 1;
	for (auto & line : content) {
		//prepare
		SpecFileLine infos;
		infos.source.line = lineId++;
		infos.source.filename = filename;

		//parse
		parseLine(infos,line,indentRef);
		out.push_back(infos);
	}
}

/*******************  FUNCTION  *********************/
void SpecFile::sanityCheckLevelHierarchy(SpecFileLineList & entries)
{
	//start as ignore comments and search first level
	int level = -1;

	//loop
	bool canLower = false;
	for (auto & line : entries) {
		//trivial
		if (line.type == SPEC_LINE_EMPTY)
			continue;

		//first non comment fix base level
		if (line.type != SPEC_LINE_EMPTY && level == -1)
			level = line.level;

		//check decrement
		if (line.level < level && canLower)
			level = line.level;

		//check
		assumeArg(line.level == level,"Invalid indentatation, get level %1, expect %2 on %3 '%4'")
			.arg(line.level)
			.arg(level)
			.arg(line.source)
			.arg(line.full)
			.end();
		
		//child
		canLower = !line.expectChild;
		if (line.expectChild)
			level++;
	}
}

/*******************  FUNCTION  *********************/
void SpecFile::applyIncludes(void)
{
	//loop to search include lines
	for (auto it = entries.begin() ; it != entries.end() ; ++it) {
		SpecFileLine entry = *it;
		if (entry.type == SPEC_LINE_COMMAND && entry.name == "include") {
			//failure
			assumeArg(entry.args.empty() == false,"Missing path to include on [inclue PATH] command at %1")
				.arg(entry.source)
				.end();

			//compute full path
			UnixPath ref = UnixPath(this->filename);
			ref.pop();
			std::string fname = UnixPath(entry.args).makeAbsolute(ref).toString();
			CDEPS_DEBUG_ARG("include","Include file at %1 : %2")
				.arg(entry.source)
				.arg(fname)
				.end();

			//load
			SpecFile included(fname);

			//increment indent
			for (auto & nentry : included.entries)
				nentry.level += entry.level;

			//insert & erase to replace
			entries.insert(it,included.entries.begin(), included.entries.end());
			entries.erase(it);

			//restart
			it = entries.begin();
		}
	}
}

/*******************  FUNCTION  *********************/
void SpecFile::dump(FileLines & out)
{
	//loop on each
	for (auto & entry : this->entries) {
		//gen indent
		stringstream buf;
		fill_n(std::ostream_iterator<char>(buf), entry.level, '\t');

		//convert to string
		buf << entry;

		//flush
		out.push_back(buf.str());
	}
}

/*******************  FUNCTION  *********************/
const SpecFileLineList & SpecFile::getEntries(void) const
{
	return entries;
}

/*******************  FUNCTION  *********************/
std::ostream & operator << (std::ostream & out, const SourceLine & source)
{
	out << source.filename << ":" << source.line;
	return out;
}

/*******************  FUNCTION  *********************/
std::ostream & operator << (std::ostream & out, const SpecFileLine & entry)
{
	//case
	switch(entry.type) {
		case SPEC_LINE_EMPTY:
			break;
		case SPEC_LINE_COMMAND:
			if (entry.args.empty())
				out << '[' << entry.name << ']';
			else
				out << '[' << entry.name << ' ' << entry.args << ']';
			break;
		case SPEC_LINE_COMMENT:
			out << '#' << entry.name;
			break;
		case SPEC_LINE_NAME:
		case SPEC_LINE_PATH:
			out << entry.name;
			break;
	}

	//has child
	if (entry.expectChild)
		out << ':';
	
	//return
	return out;
}

/*******************  FUNCTION  *********************/
void SpecFile::extractTree(SpecTreeRoot & out) const
{
	//loop on each
	for (auto it = entries.cbegin(); it != entries.cend() ; ++it) {
		const SpecFileLine & entry = *it;

		if (entry.type == SPEC_LINE_EMPTY || entry.type == SPEC_LINE_COMMENT) {
			//skip
		} else {
			//create new root element
			out.emplace_back();
			SpecFileNode & node = out.back();

			//copy
			node.def = entry;

			//load childs and jump next
			if (entry.expectChild)
				it = internalExtractSubTree(node.childs,it,entries.cend());
		}
	}
}

/*******************  FUNCTION  *********************/
SpecFileLineList::const_iterator SpecFile::internalExtractSubTree(SpecFileNodeChildList & out, SpecFileLineList::const_iterator itStart, SpecFileLineList::const_iterator itEnd)
{
	//trivial has no child
	if (itStart->expectChild == false)
		return itStart;

	//prepare return
	SpecFileLineList::const_iterator itRet = itStart;
	int expectedLevel = itStart->level + 1;

	//loop on childs
	for (auto it = ++itStart ; it != itEnd ; ++it) {
		const SpecFileLine & entry = *it;
		if (entry.type == SPEC_LINE_EMPTY || entry.type == SPEC_LINE_COMMENT) {
			//ignore
		} else if (entry.level == expectedLevel) {
			//create new element
			out.emplace_back();
			SpecFileNode & node = out.back();

			//copy
			node.def = entry;

			//load childs by recursing and jump next
			if (entry.expectChild)
				it = internalExtractSubTree(node.childs,it,itEnd);

			//update ret
			itRet = it;
		} else if (entry.level < expectedLevel) {
			break;
		} else {
			CDEPS_FATAL("Invalid tree building, level missmatch !");
		}
	}

	//return
	return itRet;
}

/*******************  FUNCTION  *********************/
void SpecFileLine::requireArgs(void) const
{
	assumeArg(args.empty() == false,"Missing arguments in '%1' at %2")
		.arg(*this)
		.arg(source)
		.end();
}

/*******************  FUNCTION  *********************/
void SpecFileLine::doAssume(bool value, const std::string & msg) const
{
	if (value == false)
		this->doFatal(msg);
}

/*******************  FUNCTION  *********************/
void SpecFileLine::doFatal(std::string msg) const
{
	msg += "Spec is '%1' at %2";
	CDEPS_FATAL_ARG(msg.c_str())
		.arg(*this)
		.arg(source)
		.end();
}

}
