/*****************************************************
			 PROJECT  : cdeps
			 VERSION  : 0.0.0-dev
			 DATE     : 11/2018
			 LICENSE  : CeCILL-C
*****************************************************/

#ifndef CDEPS_PROJECT_HPP
#define CDEPS_PROJECT_HPP

/********************  HEADERS  *********************/
#include <map>
#include "SpecFile.hpp"
#include "Package.hpp"
#include "Options.hpp"
#include "../portability/Path.hpp"

/********************  NAMESPACE  *******************/
namespace cdeps
{

/*********************  TYPES  **********************/
typedef std::list<Package> PackageList;
typedef std::map<std::string,std::string> VariableMap;

/*********************  CLASS  **********************/
class Project
{
	public:
		Project(const Options * options);
		~Project(void);
		void loadSpec(const SpecFile & spec);
		PackageList & getPackageList(void);
		const PackageList & getPackageList(void) const;
		PathList getPrefixes(void) const;
		void setVariable(const std::string & name, const std::string & value, bool append = false);
		const VariableMap & getVariables(void) const;
	private:
		static std::string getCleanVersion(void);
		void parseRootCommands(const SpecFileNode & cmd);
		void applyIfStatements(void);
		bool defineMakesOptional(const StringList & vars) const;
		static void rootCommandCheckMinCdepsReq(const SpecFileNode & cmd);
		static void rootCommandPlugins(const SpecFileNode & cmd);
	private:
		const Options * options;
		PackageList packages;
		VariableMap variables;
};

}

#endif //CDEPS_PROJECT_HPP
