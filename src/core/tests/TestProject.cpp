/*****************************************************
			 PROJECT  : cdeps
			 VERSION  : 0.0.0-dev
			 DATE     : 11/2018
			 LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
//google
#include <gtest/gtest.h>

//local
#include "../SpecFile.hpp"
#include "../Project.hpp"

/***************** USING NAMESPACE ******************/
using namespace cdeps;

/*******************  FUNCTION  *********************/
TEST(TestProject,loadFile)
{
	SpecFile file(DATADIR "/project.cdeps");
	Options options;
	Project project(&options);
	project.loadSpec(file);
}

/*******************  FUNCTION  *********************/
TEST(TestProject,loadContent_fail_minimum_required)
{
	FileLines content;
	content.push_back("[cdeps_minimum_required 2000.40000.30000]");

	SpecFile file;
	file.loadContent(content,"none.none");

	Options options;
	Project project(&options);
	EXPECT_EXIT(project.loadSpec(file), ::testing::ExitedWithCode(1), "version is too old");
}

/*******************  FUNCTION  *********************/
TEST(TestProject,loadContent_invalid_plugin)
{
	FileLines content;
	content.push_back("[plugins]:");
	content.push_back("\tbob");

	SpecFile file;
	file.loadContent(content,"none.none");

	Options options;
	Project project(&options);
	EXPECT_EXIT(project.loadSpec(file), ::testing::ExitedWithCode(1), "Unknown plugin");
}

/*******************  FUNCTION  *********************/
TEST(TestProject,applyIfStatements)
{
	FileLines content;
	content.push_back("ZeroMQ:");
	content.push_back("\t/include/stdio.h:");
	content.push_back("\t\tprintf");

	SpecFile spec;
	spec.loadContent(content,"none.none");

	Options options;
	Project project(&options);
	project.loadSpec(spec);

	EXPECT_FALSE(project.getPackageList().front().optional);
	EXPECT_FALSE(project.getPackageList().front().files.front().optional);
	EXPECT_FALSE(project.getPackageList().front().files.front().symbols.front().optional);
}

/*******************  FUNCTION  *********************/
TEST(TestProject,applyIfStatements_if_all_not_def)
{
	FileLines content;
	content.push_back("ZeroMQ:");
	content.push_back("\t[if USE_ZMQ]");
	content.push_back("\t/include/stdio.h:");
	content.push_back("\t\t[if USE_ZMQ_STDIO]");
	content.push_back("\t\tprintf:");
	content.push_back("\t\t\t[if USE_ZMQ_STDIO_PRINTF]");

	SpecFile spec;
	spec.loadContent(content,"none.none");

	Options options;
	Project project(&options);
	project.loadSpec(spec);

	EXPECT_TRUE(project.getPackageList().front().optional);
	EXPECT_TRUE(project.getPackageList().front().files.front().optional);
	EXPECT_TRUE(project.getPackageList().front().files.front().symbols.front().optional);
}

/*******************  FUNCTION  *********************/
TEST(TestProject,applyIfStatements_if_all_def)
{
	FileLines content;
	content.push_back("ZeroMQ:");
	content.push_back("\t[if USE_ZMQ]");
	content.push_back("\t/include/stdio.h:");
	content.push_back("\t\t[if USE_ZMQ_STDIO]");
	content.push_back("\t\tprintf:");
	content.push_back("\t\t\t[if USE_ZMQ_STDIO_PRINTF]");

	SpecFile spec;
	spec.loadContent(content,"none.none");

	Options options;
	options.defines.push_back("USE_ZMQ");
	options.defines.push_back("USE_ZMQ_STDIO");
	options.defines.push_back("USE_ZMQ_STDIO_PRINTF");

	Project project(&options);
	project.loadSpec(spec);

	EXPECT_FALSE(project.getPackageList().front().optional);
	EXPECT_FALSE(project.getPackageList().front().files.front().optional);
	EXPECT_FALSE(project.getPackageList().front().files.front().symbols.front().optional);
}

