/*****************************************************
			 PROJECT  : cdeps
			 VERSION  : 0.0.0-dev
			 DATE     : 11/2018
			 LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <gtest/gtest.h>
#include "../Options.hpp"

/***************** USING NAMESPACE ******************/
using namespace cdeps;

/*******************  FUNCTION  *********************/
TEST(TestOptions,parse_1)
{
	//vars
	Options options;
	const int cnt = 14;
	const char * argv[cnt] = {
		"prgm",
		"-G","text",
		"-f","test.cdeps",
		"-D","TEST1",
		"--define","TEST2",
		"-p","/home/bob/usr",
		"--prefix","/home/foo/usr",
		NULL
	};

	//setenv
	setenv("CDEPS_PREFIXES","/my_nice_prefix1/usr/:/another_prefix/usr/",true);

	//parse
	options.loadEnv();
	options.parseArgs(cnt-1,(char**)argv);

	//ref
	StringList ref;
	ref.push_back("TEST1");
	ref.push_back("TEST2");
	#ifdef __x86_64__
		ref.push_back("ARCH_X86_64");
	#endif

	//ref
	StringList refPrefix;
	refPrefix.push_back("/my_nice_prefix1/usr/");
	refPrefix.push_back("/another_prefix/usr/");
	refPrefix.push_back("/home/bob/usr");
	refPrefix.push_back("/home/foo/usr");

	//check
	EXPECT_EQ(options.generator,CDEPS_GENERATOR_TEXT);
	EXPECT_EQ(options.filename,"test.cdeps");
	
	//loop
	ASSERT_EQ(options.defines.size(),ref.size());
	auto itRef = ref.begin();
	for (auto & it : options.defines)
		EXPECT_EQ(it,*(itRef++));

	//loop
	ASSERT_EQ(options.prefixes.size(),refPrefix.size());
	auto itRefPref = refPrefix.begin();
	for (auto & it : options.prefixes)
		EXPECT_EQ(it,*(itRefPref++));
}