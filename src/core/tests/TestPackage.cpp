/*****************************************************
			 PROJECT  : cdeps
			 VERSION  : 0.0.0-dev
			 DATE     : 11/2018
			 LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <gtest/gtest.h>
#include "../Package.hpp"

/***************** USING NAMESPACE ******************/
using namespace cdeps;

/*******************  FUNCTION  *********************/
TEST(TestPackageInfos,construct)
{
	PackageInfos infos;
	//check
	EXPECT_EQ(infos.fullName,"");
	EXPECT_EQ(infos.url,"");
	EXPECT_EQ(infos.brief,"");
}

/*******************  FUNCTION  *********************/
TEST(TestPackageInfos,loadFromTree)
{
	//text
	FileLines content;
	content.push_back("infos:");
	content.push_back("\t[name MyFullName EventWithSpace]");
	content.push_back("\t[url http://www.project.org/]");
	content.push_back("\t[brief This is my project biref doc, event with brackets ([]) in !]");

	//parse
	SpecFile spec;
	spec.loadContent(content,"none.none");

	//build tree
	SpecTreeRoot tree;
	spec.extractTree(tree);
	ASSERT_EQ(tree.size(),1);
	
	//parse
	PackageInfos infos;
	infos.loadFromTree(tree.front());

	//check
	EXPECT_EQ(infos.fullName,"MyFullName EventWithSpace");
	EXPECT_EQ(infos.url,"http://www.project.org/");
	EXPECT_EQ(infos.brief,"This is my project biref doc, event with brackets ([]) in !");
}

/*******************  FUNCTION  *********************/
TEST(TestPackExpectedFileSymbol,construct)
{
	PackExpectedFileSymbol symbol;

	//check
	EXPECT_TRUE(symbol.name.empty());
	EXPECT_FALSE(symbol.optional);
	EXPECT_TRUE(symbol.ifHasVar.empty());
	EXPECT_TRUE(symbol.hasVars.empty());
	EXPECT_TRUE(symbol.components.empty());
}

/*******************  FUNCTION  *********************/
TEST(TestPackage,loadFromTree)
{
	//text
	FileLines content;
	content.push_back("ZeroMQ:");
	content.push_back("\t[optional HAS_ZMQ_INIT]");
	content.push_back("\t[if ENABLE_ZMQ]");
	content.push_back("\t[if ENABLE_ZMQ_ALL]");
	content.push_back("\t[set ZMQ_PACKAGE_NAME]");
	content.push_back("\t[set-append ZMQ_PACKAGE_NAME_APPEND]");

	//parse
	SpecFile spec;
	spec.loadContent(content,"none.none");

	//build tree
	SpecTreeRoot tree;
	spec.extractTree(tree);
	ASSERT_EQ(tree.size(),1);
	
	//parse
	Package pack;
	pack.loadFromTree(tree.front());

	//check
	EXPECT_EQ(pack.name,"ZeroMQ");
	EXPECT_TRUE(pack.optional);
	EXPECT_EQ(pack.ifHasVar.size(),2);
	EXPECT_EQ(pack.ifHasVar.front(),"ENABLE_ZMQ");
	EXPECT_EQ(pack.ifHasVar.back(),"ENABLE_ZMQ_ALL");
	EXPECT_EQ(pack.hasVars.size(),1);
	EXPECT_EQ(pack.hasVars.front(),"HAS_ZMQ_INIT");
	EXPECT_EQ(pack.setVars.size(),2);
	EXPECT_EQ(pack.setVars.front().name,"ZMQ_PACKAGE_NAME");
	EXPECT_EQ(pack.setVars.front().append,false);
	EXPECT_EQ(pack.setVars.back().name,"ZMQ_PACKAGE_NAME_APPEND");
	EXPECT_EQ(pack.setVars.back().append,true);
}

/*******************  FUNCTION  *********************/
TEST(TestPackExpectedFileSymbol,loadFromTree)
{
	//text
	FileLines content;
	content.push_back("zmqInit:");
	content.push_back("\t[optional HAS_ZMQ_INIT]");
	content.push_back("\t[if ENABLE_ZMQ]");
	content.push_back("\t[if ENABLE_ZMQ_ALL]");
	content.push_back("\t[set ZMQ_INIT_SYMBOL]");
	content.push_back("\t[set-append ZMQ_INIT_SYMBOL_APPEND]");
	content.push_back("\t[is function]");
	content.push_back("\t[lang cpp]");
	content.push_back("\t[component core]");

	//parse
	SpecFile spec;
	spec.loadContent(content,"none.none");

	//build tree
	SpecTreeRoot tree;
	spec.extractTree(tree);
	ASSERT_EQ(tree.size(),1);
	
	//parse
	PackExpectedFileSymbol symbol;
	symbol.loadFromTree(tree.front());

	//check
	EXPECT_EQ(symbol.name,"zmqInit");
	EXPECT_TRUE(symbol.optional);
	EXPECT_EQ(symbol.ifHasVar.size(),2);
	EXPECT_EQ(symbol.ifHasVar.front(),"ENABLE_ZMQ");
	EXPECT_EQ(symbol.ifHasVar.back(),"ENABLE_ZMQ_ALL");
	EXPECT_EQ(symbol.hasVars.size(),1);
	EXPECT_EQ(symbol.hasVars.front(),"HAS_ZMQ_INIT");
	EXPECT_EQ(symbol.setVars.size(),2);
	EXPECT_EQ(symbol.setVars.front().name,"ZMQ_INIT_SYMBOL");
	EXPECT_EQ(symbol.setVars.front().append,false);
	EXPECT_EQ(symbol.setVars.back().name,"ZMQ_INIT_SYMBOL_APPEND");
	EXPECT_EQ(symbol.setVars.back().append,true);
	EXPECT_EQ(symbol.components.size(),1);
	EXPECT_EQ(symbol.components.front(),"core");
	EXPECT_EQ(symbol.type,CDEPS_SYMBOL_FUNCTION);
	EXPECT_EQ(symbol.lang,CDEPS_LANG_CPP);
}

/*******************  FUNCTION  *********************/
TEST(TestPackageExpectedFile,construct)
{
	PackageExpectedFile file;

	//check
	EXPECT_TRUE(file.path.empty());
	EXPECT_FALSE(file.optional);
	EXPECT_TRUE(file.ifHasVar.empty());
	EXPECT_TRUE(file.hasVars.empty());
	EXPECT_TRUE(file.components.empty());
	EXPECT_TRUE(file.setVars.empty());
	EXPECT_FALSE(file.sendToVersionExtractor);
}

/*******************  FUNCTION  *********************/
TEST(TestPackageExpectedFile,loadFromTree)
{
	//text
	FileLines content;
	content.push_back("/include/zmq.h:");
	content.push_back("\t[optional HAS_ZMQ_INIT]");
	content.push_back("\t[if ENABLE_ZMQ]");
	content.push_back("\t[if ENABLE_ZMQ_ALL]");
	content.push_back("\t[component core]");
	content.push_back("\t[set ZMQ_HEADER]");
	content.push_back("\t[set-append ZMQ_HEADER_APPEND]");
	content.push_back("\t[send-to-version-extractor]");
	content.push_back("\t[dir /usr/src/linux/include]");
	content.push_back("\tinitZmq");
	content.push_back("\tfiniZmq");

	//parse
	SpecFile spec;
	spec.loadContent(content,"none.none");

	//build tree
	SpecTreeRoot tree;
	spec.extractTree(tree);
	ASSERT_EQ(tree.size(),1);
	
	//parse
	PackageExpectedFile file;
	file.loadFromTree(tree.front());

	//check
	EXPECT_EQ(file.path,"/include/zmq.h");
	EXPECT_TRUE(file.optional);
	EXPECT_TRUE(file.sendToVersionExtractor);
	EXPECT_EQ(file.ifHasVar.size(),2);
	EXPECT_EQ(file.ifHasVar.front(),"ENABLE_ZMQ");
	EXPECT_EQ(file.ifHasVar.back(),"ENABLE_ZMQ_ALL");
	EXPECT_EQ(file.hasVars.size(),1);
	EXPECT_EQ(file.hasVars.front(),"HAS_ZMQ_INIT");
	EXPECT_EQ(file.components.size(),1);
	EXPECT_EQ(file.components.front(),"core");
	EXPECT_EQ(file.setVars.size(),2);
	EXPECT_EQ(file.setVars.front().name,"ZMQ_HEADER");
	EXPECT_EQ(file.setVars.front().append,false);
	EXPECT_EQ(file.setVars.back().name,"ZMQ_HEADER_APPEND");
	EXPECT_EQ(file.setVars.back().append,true);
	EXPECT_EQ(file.extraSearchPaths.size(),1);
	EXPECT_EQ(file.extraSearchPaths.front(),"/usr/src/linux/include");
	EXPECT_EQ(file.symbols.size(),2);
}

/*******************  FUNCTION  *********************/
TEST(TestPackage,symbolLangFromString)
{
	SourceLine source = {
		"none.none",
		10
	};

	EXPECT_EQ(symbolLangFromString("C",source),CDEPS_LANG_C);
	EXPECT_EQ(symbolLangFromString("c",source),CDEPS_LANG_C);

	EXPECT_EQ(symbolLangFromString("cpp",source),CDEPS_LANG_CPP);
	EXPECT_EQ(symbolLangFromString("c++",source),CDEPS_LANG_CPP);
	EXPECT_EQ(symbolLangFromString("CPP",source),CDEPS_LANG_CPP);

	EXPECT_EXIT(symbolLangFromString("none",source), ::testing::ExitedWithCode(1), "Invalid language");
}

/*******************  FUNCTION  *********************/
TEST(TestPackage,symbolTypeFromString)
{
	SourceLine source = {
		"none.none",
		10
	};

	EXPECT_EQ(symbolTypeFromString("function",source),CDEPS_SYMBOL_FUNCTION);
	EXPECT_EQ(symbolTypeFromString("func",source),CDEPS_SYMBOL_FUNCTION);

	EXPECT_EQ(symbolTypeFromString("variable",source),CDEPS_SYMBOL_VARIABLE);
	EXPECT_EQ(symbolTypeFromString("var",source),CDEPS_SYMBOL_VARIABLE);

	EXPECT_EQ(symbolTypeFromString("macro",source),CDEPS_SYMBOL_MACRO);

	EXPECT_EXIT(symbolTypeFromString("none",source), ::testing::ExitedWithCode(1), "Invalid symbol type");
}
