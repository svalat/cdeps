/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
//std
#include <iostream>

//google
#include <gtest/gtest.h>

//local
#include "../common/Debug.hpp"
#include "../SpecFile.hpp"
#include "../common/Helper.hpp"

/***************** USING NAMESPACE ******************/
using namespace cdeps;
using namespace std;

/*******************  FUNCTION  *********************/
TEST(SpecFile,getIndentString_no_indent) {
	EXPECT_EQ("",SpecFile::getIndentString("test"));
}

/*******************  FUNCTION  *********************/
TEST(SpecFile,getIndentString_spaces) {
	EXPECT_EQ("  ",SpecFile::getIndentString("  test"));
	EXPECT_EQ("    ",SpecFile::getIndentString("    test"));
}

/*******************  FUNCTION  *********************/
TEST(SpecFile,getIndentString_tabs) {
	EXPECT_EQ("\t",SpecFile::getIndentString("\ttest"));
	EXPECT_EQ("\t\t",SpecFile::getIndentString("\t\ttest"));
}

/*******************  FUNCTION  *********************/
TEST(SpecFile,getIndentString_mix) {
	EXPECT_EQ("\t    \t",SpecFile::getIndentString("\t    \ttest"));
}

/*******************  FUNCTION  *********************/
TEST(SpecFile,getIndentString_empty_line) {
	//empty
	EXPECT_EQ("",SpecFile::getIndentString(""));
	EXPECT_EQ("\t",SpecFile::getIndentString("\t"));
	EXPECT_EQ("    ",SpecFile::getIndentString("    "));
}

/*******************  FUNCTION  *********************/
TEST(SpecFile,getIndentLevel_no_indent) {
	EXPECT_EQ(0,SpecFile::getIndentLevel("","    "));
	EXPECT_EQ(0,SpecFile::getIndentLevel("","\t"));
}

/*******************  FUNCTION  *********************/
TEST(SpecFile,getIndentLevel_one_indent) {
	EXPECT_EQ(1,SpecFile::getIndentLevel("    ","    "));
	EXPECT_EQ(1,SpecFile::getIndentLevel("\t","\t"));
}

/*******************  FUNCTION  *********************/
TEST(SpecFile,getIndentLevel_two_indent) {
	EXPECT_EQ(2,SpecFile::getIndentLevel("        ","    "));
	EXPECT_EQ(2,SpecFile::getIndentLevel("\t\t","\t"));
}

/*******************  FUNCTION  *********************/
TEST(SpecFile,getIndentLevel_two_indent_mix) {
	EXPECT_EQ(2,SpecFile::getIndentLevel("\t    \t    ","\t    "));
}

/*******************  FUNCTION  *********************/
TEST(SpecFile,getIndentLevel_invalid) {
	EXPECT_EQ(-1,SpecFile::getIndentLevel("   ","    "));
	EXPECT_EQ(-1,SpecFile::getIndentLevel("\t","    "));
	EXPECT_EQ(-1,SpecFile::getIndentLevel("       ","    "));
	EXPECT_EQ(-1,SpecFile::getIndentLevel("    \t","    "));
}

/*******************  FUNCTION  *********************/
TEST(SpecFile,getIdentRef_tab_valid) {
	const char * value = "no-indent\n\tindent1\n\tindent1\n\t\tindent2";
	FileLines content;
	Helper::loadStringLines(content,value);

	std::string ref = SpecFile::getIndentRef(content,"none.none");

	EXPECT_EQ(ref,"\t");
}

/*******************  FUNCTION  *********************/
TEST(SpecFile,getIdentRef_double_tab_valid) {
	const char * value = "no-indent\n\t\tindent1\n\t\tindent1\n\t\t\t\tindent2";
	FileLines content;
	Helper::loadStringLines(content,value);

	std::string ref = SpecFile::getIndentRef(content,"none.none");

	EXPECT_EQ(ref,"\t\t");
}

/*******************  FUNCTION  *********************/
TEST(SpecFile,getIdentRef_spaces) {
	const char * value = "no-indent\n    indent1\n    indent1\n        indent2";
	FileLines content;
	Helper::loadStringLines(content,value);

	std::string ref = SpecFile::getIndentRef(content,"none.none");

	EXPECT_EQ(ref,"    ");
}

/*******************  FUNCTION  *********************/
TEST(SpecFile,getIdentRef_spaces_invalid) {
	const char * value = "no-indent\n    indent1\n    indent1\n      indent2";
	FileLines content;
	Helper::loadStringLines(content,value);

	EXPECT_EXIT(SpecFile::getIndentRef(content,"none.none"), ::testing::ExitedWithCode(1), "Invalid indentation");
}

/*******************  FUNCTION  *********************/
TEST(SpecFile,parseLine_comment) {
	const char * value  = "    #comment line";
	const char * indent = "    ";
	const std::string fname = "none.none";
	SpecFileLine infos;
	infos.source.filename = fname;
	infos.source.line = 0;

	SpecFile::parseLine(infos,value,indent);

	EXPECT_EQ(infos.type,SPEC_LINE_COMMENT);
	EXPECT_EQ(infos.name,"comment line");
	EXPECT_EQ(infos.args,"");
	EXPECT_EQ(infos.level,1);
	EXPECT_EQ(infos.expectChild,false);
}

/*******************  FUNCTION  *********************/
TEST(SpecFile,parseLine_name) {
	const char * value  = "    prefix";
	const char * indent = "    ";
	const std::string fname = "none.none";
	SpecFileLine infos;
	infos.source.filename = fname;
	infos.source.line = 0;

	SpecFile::parseLine(infos,value,indent);

	EXPECT_EQ(infos.type,SPEC_LINE_NAME);
	EXPECT_EQ(infos.name,"prefix");
	EXPECT_EQ(infos.args,"");
	EXPECT_EQ(infos.level,1);
	EXPECT_EQ(infos.expectChild,false);
}

/*******************  FUNCTION  *********************/
TEST(SpecFile,parseLine_name_child) {
	const char * value  = "    prefix:";
	const char * indent = "    ";
	const std::string fname = "none.none";
	SpecFileLine infos;
	infos.source.filename = fname;
	infos.source.line = 0;

	SpecFile::parseLine(infos,value,indent);

	EXPECT_EQ(infos.type,SPEC_LINE_NAME);
	EXPECT_EQ(infos.name,"prefix");
	EXPECT_EQ(infos.args,"");
	EXPECT_EQ(infos.level,1);
	EXPECT_EQ(infos.expectChild,true);
}

/*******************  FUNCTION  *********************/
TEST(SpecFile,parseLine_command_no_arg) {
	const char * value  = "    [no-prefix]";
	const char * indent = "    ";
	const std::string fname = "none.none";
	SpecFileLine infos;
	infos.source.filename = fname;
	infos.source.line = 0;

	SpecFile::parseLine(infos,value,indent);

	EXPECT_EQ(infos.type,SPEC_LINE_COMMAND);
	EXPECT_EQ(infos.name,"no-prefix");
	EXPECT_EQ(infos.args,"");
	EXPECT_EQ(infos.level,1);
	EXPECT_EQ(infos.expectChild,false);
}

/*******************  FUNCTION  *********************/
TEST(SpecFile,parseLine_command_no_arg_child) {
	const char * value  = "    [no-prefix]:";
	const char * indent = "    ";
	const std::string fname = "none.none";
	SpecFileLine infos;
	infos.source.filename = fname;
	infos.source.line = 0;

	SpecFile::parseLine(infos,value,indent);

	EXPECT_EQ(infos.type,SPEC_LINE_COMMAND);
	EXPECT_EQ(infos.name,"no-prefix");
	EXPECT_EQ(infos.args,"");
	EXPECT_EQ(infos.level,1);
	EXPECT_EQ(infos.expectChild,true);
}

/*******************  FUNCTION  *********************/
TEST(SpecFile,parseLine_command_one_arg) {
	const char * value  = "[cdeps_minimum_required 1.0.0]";
	const char * indent = "    ";
	const std::string fname = "none.none";
	SpecFileLine infos;
	infos.source.filename = fname;
	infos.source.line = 0;

	SpecFile::parseLine(infos,value,indent);

	EXPECT_EQ(infos.type,SPEC_LINE_COMMAND);
	EXPECT_EQ(infos.full,value);
	EXPECT_EQ(infos.name,"cdeps_minimum_required");
	EXPECT_EQ(infos.args,"1.0.0");
	EXPECT_EQ(infos.level,0);
	EXPECT_EQ(infos.expectChild,false);
}

/*******************  FUNCTION  *********************/
TEST(SpecFile,parseLine_command_one_arg_tabs) {
	const char * value  = "[cdeps_minimum_required\t\t1.0.0]";
	const char * indent = "    ";
	const std::string fname = "none.none";
	SpecFileLine infos;
	infos.source.filename = fname;
	infos.source.line = 0;

	SpecFile::parseLine(infos,value,indent);

	EXPECT_EQ(infos.type,SPEC_LINE_COMMAND);
	EXPECT_EQ(infos.name,"cdeps_minimum_required");
	EXPECT_EQ(infos.args,"1.0.0");
	EXPECT_EQ(infos.level,0);
	EXPECT_EQ(infos.expectChild,false);
}

/*******************  FUNCTION  *********************/
TEST(SpecFile,parseLine_command_many_args) {
	const char * value  = "[cdeps_minimum_required 1.0.0 2.0.0 3.0.0]";
	const char * indent = "    ";
	const std::string fname = "none.none";
	SpecFileLine infos;
	infos.source.filename = fname;
	infos.source.line = 0;

	SpecFile::parseLine(infos,value,indent);

	EXPECT_EQ(infos.type,SPEC_LINE_COMMAND);
	EXPECT_EQ(infos.name,"cdeps_minimum_required");
	EXPECT_EQ(infos.args,"1.0.0 2.0.0 3.0.0");
	EXPECT_EQ(infos.level,0);
	EXPECT_EQ(infos.expectChild,false);
}

/*******************  FUNCTION  *********************/
TEST(SpecFile,parseLine_path) {
	const char * value  = "    /include/glibc.h";
	const char * indent = "    ";
	const std::string fname = "none.none";
	SpecFileLine infos;
	infos.source.filename = fname;
	infos.source.line = 0;

	SpecFile::parseLine(infos,value,indent);

	EXPECT_EQ(infos.type,SPEC_LINE_PATH);
	EXPECT_EQ(infos.name,"/include/glibc.h");
	EXPECT_EQ(infos.args,"");
	EXPECT_EQ(infos.level,1);
	EXPECT_EQ(infos.expectChild,false);
}

/*******************  FUNCTION  *********************/
TEST(SpecFile,parseLine_path_child) {
	const char * value  = "    /include/glibc.h:";
	const char * indent = "    ";
	const std::string fname = "none.none";
	SpecFileLine infos;
	infos.source.filename = fname;
	infos.source.line = 0;

	SpecFile::parseLine(infos,value,indent);

	EXPECT_EQ(infos.type,SPEC_LINE_PATH);
	EXPECT_EQ(infos.name,"/include/glibc.h");
	EXPECT_EQ(infos.args,"");
	EXPECT_EQ(infos.level,1);
	EXPECT_EQ(infos.expectChild,true);
}

/*******************  FUNCTION  *********************/
TEST(SpecFile,parseLine_empty) {
	const char * value  = "        ";
	const char * indent = "    ";
	const std::string fname = "none.none";
	SpecFileLine infos;
	infos.source.filename = fname;
	infos.source.line = 0;

	SpecFile::parseLine(infos,value,indent);

	EXPECT_EQ(infos.type,SPEC_LINE_EMPTY);
	EXPECT_EQ(infos.full,value);
	EXPECT_EQ(infos.name,"");
	EXPECT_EQ(infos.args,"");
	EXPECT_EQ(infos.level,2);
	EXPECT_EQ(infos.expectChild,false);
}

/*******************  FUNCTION  *********************/
TEST(SpecFile,sanityCheckLevelHierarchy_ok) {
	const std::string value = "\n\
	#!/usr/bin/cdeps\n\
	\n\
	[global-features]:\n\
		version-extractor:\n\
			[url-dir http://cepsrepo.org/version-extractor/v1/]\n\
			[no-prefix]\n\
		cmake:\n\
			# output directory\n\
			[directory /cmake]\n\
	\n\
	ZeroMQ:\n\
		/include/zmq.h\n\
		/include/zz.h\n\
		/lib/zmq\n\
	";
	const std::string fname = "none.none";

	FileLines content;
	SpecFileLineList out;
	Helper::loadStringLines(content,value);
	SpecFile::parseLines(out,content,fname);
	SpecFile::sanityCheckLevelHierarchy(out);
}

/*******************  FUNCTION  *********************/
TEST(SpecFile,sanityCheckLevelHierarchy_invalid_1) {
	const std::string value = "\n\
	#!/usr/bin/cdeps\n\
	\n\
	[global-features]:\n\
		version-extractor:\n\
			[url-dir http://cepsrepo.org/version-extractor/v1/]\n\
			[no-prefix]\n\
		cmake:\n\
		# output directory\n\
		[directory /cmake]\n\
	\n\
	ZeroMQ:\n\
		/include/zmq.h\n\
		/include/zz.h\n\
		/lib/zmq\n\
	";
	const std::string fname = "none.none";

	FileLines content;
	SpecFileLineList out;
	Helper::loadStringLines(content,value);
	SpecFile::parseLines(out,content,fname);
	EXPECT_EXIT(SpecFile::sanityCheckLevelHierarchy(out), ::testing::ExitedWithCode(1), "Invalid indentatation");
}

/*******************  FUNCTION  *********************/
TEST(SpecFile,sanityCheckLevelHierarchy_invalid_2) {
	const std::string value = "\n\
	#!/usr/bin/cdeps\n\
	\n\
	[global-features]:\n\
		version-extractor:\n\
			[url-dir http://cepsrepo.org/version-extractor/v1/]\n\
			[no-prefix]\n\
		cmake:\n\
				# output directory\n\
				[directory /cmake]\n\
	\n\
	ZeroMQ:\n\
		/include/zmq.h\n\
		/include/zz.h\n\
		/lib/zmq\n\
	";
	const std::string fname = "none.none";

	FileLines content;
	SpecFileLineList out;
	Helper::loadStringLines(content,value);
	SpecFile::parseLines(out,content,fname);
	EXPECT_EXIT(SpecFile::sanityCheckLevelHierarchy(out), ::testing::ExitedWithCode(1), "Invalid indentatation");
}

/*******************  FUNCTION  *********************/
TEST(SpecFile,sanityCheckLevelHierarchy_invalid_3) {
	const std::string value = "\n\
	#!/usr/bin/cdeps\n\
	\n\
	[global-features]:\n\
		version-extractor:\n\
			[url-dir http://cepsrepo.org/version-extractor/v1/]\n\
			[no-prefix]\n\
		cmake:\n\
			# output directory\n\
			[directory /cmake]\n\
	\n\
	ZeroMQ\n\
		/include/zmq.h\n\
		/include/zz.h\n\
		/lib/zmq\n\
	";
	const std::string fname = "none.none";

	FileLines content;
	SpecFileLineList out;
	Helper::loadStringLines(content,value);
	SpecFile::parseLines(out,content,fname);
	EXPECT_EXIT(SpecFile::sanityCheckLevelHierarchy(out), ::testing::ExitedWithCode(1), "Invalid indentatation");
}

/*******************  FUNCTION  *********************/
TEST(SpecFile,include) {
	//load
	const std::string fname = DATADIR "/root.cdeps";
	SpecFile spec(fname);

	//re-extract lines
	FileLines content;
	spec.dump(content);

	//ref
	const char * exp[] = {
		"#before include",
		"#this is included 1",
		"#this is included 2",
		"#after include",
		"",
		"root:",
		"\t#before include indented",
		"\t#this is included 1",
		"\t#this is included 2",
		"\t#after include indented",
		"",
		"#end",
		"----------- END -------------"
	};

	//display
	//for (auto & it : content)
	//	cout << it << endl;
	
	//check
	int i = 0;
	for (auto & it : content) {
		ASSERT_NE(exp[i],"----------- END -------------") << "Line:" << i;
		EXPECT_EQ(it,exp[i++]) << "Line: " << i;
	}
}

/*******************  FUNCTION  *********************/
TEST(SpecFile,extractTree) {
	const std::string value = "\n\
	#!/usr/bin/cdeps\n\
	\n\
	[global-features]:\n\
		version-extractor:\n\
			[url-dir http://cepsrepo.org/version-extractor/v1/]\n\
			[no-prefix]\n\
		cmake:\n\
			# output directory\n\
			[directory /cmake]\n\
	\n\
	ZeroMQ:\n\
		/include/zmq.h\n\
		/include/zz.h\n\
		/lib/zmq\n\
	";
	const std::string fname = "none.none";

	//convert
	FileLines content;
	SpecFileLineList out;
	Helper::loadStringLines(content,value);
	
	//load content
	SpecFile file;
	file.loadContent(content,fname);

	//build tree
	SpecTreeRoot root;
	file.extractTree(root);

	//check root elements
	EXPECT_EQ(root.size(),2);
}

