/*****************************************************
			 PROJECT  : cdeps
			 VERSION  : 0.0.0-dev
			 DATE     : 11/2018
			 LICENSE  : CeCILL-C
*****************************************************/

#ifndef CDEPS_PACKAGE_HPP
#define CDEPS_PACKAGE_HPP

/********************  HEADERS  *********************/
//std
#include <string>

//local
#include "../common/Helper.hpp"
#include "SpecFile.hpp"

/********************  NAMESPACE  *******************/
namespace cdeps
{

/*********************  STRUCT  *********************/
struct PackageInfos
{
	//vars
	std::string fullName;
	std::string url;
	std::string brief;
	//loader
	void loadFromTree(const SpecFileNode & node);
};

/*********************  STRUCT  *********************/
struct ExportedVariable
{
	ExportedVariable(std::string name, bool append);
	std::string name;
	bool append;
};

/*********************  TYPES  **********************/
typedef std::list<ExportedVariable> ExportedVariableList;

/**********************  ENUM  **********************/
enum PackageSymbolType
{
	CDEPS_SYMBOL_TYPE,
	CDEPS_SYMBOL_FUNCTION,
	CDEPS_SYMBOL_MACRO,
	CDEPS_SYMBOL_VARIABLE,
	CDEPS_SYMBOL_MAX_ID
};

/**********************  ENUM  **********************/
enum PackageSymbolLang
{
	CDEPS_LANG_C,
	CDEPS_LANG_CPP,
};

/*********************  STRUCT  *********************/
struct PackExpectedFileSymbol
{
	//vars
	std::string name;
	bool optional;
	bool found = true;
	PackageSymbolType type;
	PackageSymbolLang lang;
	ExportedVariableList setVars;
	StringList ifHasVar;
	StringList hasVars;
	StringList components;
	SourceLine source;
	//loader
	PackExpectedFileSymbol(void);
	void loadFromTree(const SpecFileNode & node);
	void loadCmdFromSpecLine(const SpecFileLine & cmd);
};

/*********************  TYPES  **********************/
typedef std::list<PackExpectedFileSymbol> PackExpectedFileSymbolList;

/*********************  STRUCT  *********************/
struct PackageExpectedFile
{
	//vars
	std::string path;
	bool sendToVersionExtractor;
	bool optional;
	StringList components;
	ExportedVariableList setVars;
	StringList hasVars;
	StringList ifHasVar;
	StringList extraSearchPaths;
	PackExpectedFileSymbolList symbols;
	bool found;
	std::string foundAt;
	SourceLine source;
	//loader
	PackageExpectedFile(void);
	void loadFromTree(const SpecFileNode & node);
	void loadCmdFromSpecLine(const SpecFileLine & cmd);
};

/*********************  TYPES  **********************/
typedef std::list<PackageExpectedFile> PackageExpectedFileList;

/*********************  STRUCT  *********************/
struct Package
{
	std::string name;
	bool optional;
	bool found;
	ExportedVariableList setVars;
	StringList hasVars;
	StringList ifHasVar;
	PackageInfos infos;
	PackageExpectedFileList files;
	SourceLine source;
	//funcs
	Package(void);
	void loadFromTree(const SpecFileNode & node);
};

/*******************  FUNCTION  *********************/
PackageSymbolLang symbolLangFromString(const std::string & value, const SourceLine & source);
PackageSymbolType symbolTypeFromString(const std::string & value, const SourceLine & source);

}

#endif //CDEPS_PACKAGE_HPP
