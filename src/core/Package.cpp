/*****************************************************
			 PROJECT  : cdeps
			 VERSION  : 0.0.0-dev
			 DATE     : 11/2018
			 LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include "../common/Debug.hpp"
#include "Package.hpp"

/********************  NAMESPACE  *******************/
namespace cdeps
{

/*******************  FUNCTION  *********************/
ExportedVariable::ExportedVariable(std::string name, bool append)
{
	this->name = name;
	this->append = append;
}

/*******************  FUNCTION  *********************/
Package::Package(void)
{
	this->optional = false;
	this->found = true;
}

/*******************  FUNCTION  *********************/
void Package::loadFromTree(const SpecFileNode & node)
{
	//check
	assume(node.def.type == SPEC_LINE_NAME,"Invalid node type, expect NAME");

	//extract name
	this->name = node.def.name;
	this->source = node.def.source;

	//loop on childs
	for (auto & child : node.childs) {
		if (child.def.type == SPEC_LINE_COMMAND && child.def.name == "optional") {
			this->optional = true;
			if (child.def.args.empty() == false)
				this->hasVars.push_back(child.def.args);
		} else if (child.def.type == SPEC_LINE_COMMAND && child.def.name == "if") {
			child.def.requireArgs();
			this->ifHasVar.push_back(child.def.args);
		} else if (child.def.type == SPEC_LINE_COMMAND && child.def.name == "set") {
			child.def.requireArgs();
			this->setVars.push_back(ExportedVariable(child.def.args,false));
		} else if (child.def.type == SPEC_LINE_COMMAND && child.def.name == "set-append") {
			child.def.requireArgs();
			this->setVars.push_back(ExportedVariable(child.def.args,true));
		} else if (child.def.type == SPEC_LINE_NAME && child.def.name == "infos") {
			//info node
			this->infos.loadFromTree(child);
		} else if (child.def.type == SPEC_LINE_PATH) {
			//path to find
			this->files.emplace_back();
			this->files.back().loadFromTree(child);
		} else {
			//mistake
			CDEPS_FATAL_ARG("Invalid node in package : %1 at %2")
				.arg(child.def)
				.arg(child.def.source)
				.end();
		}
	}
}

/*******************  FUNCTION  *********************/
void PackageInfos::loadFromTree(const SpecFileNode & node)
{
	//checks
	assumeArg(node.def.type == SPEC_LINE_NAME && node.def.name == "infos","Invalid de type, expect 'infos:' get '%1' at %2")
		.arg(node.def)
		.arg(node.def.source)
		.end();

	//loop on all
	for (auto & child : node.childs) {
		//child should be command
		assumeArg(child.def.type == SPEC_LINE_COMMAND,"Invalid de type, expect 'infos:' get '%1' at %2")
			.arg(child.def)
			.arg(child.def.source)
			.end();
		
		//all expect a arg value
		assumeArg(child.def.type == SPEC_LINE_COMMAND,"Command expect a parameter to set values, get '%1' at %2")
			.arg(child.def)
			.arg(child.def.source)
			.end();
		
		//switch commands
		if (child.def.name == "name") {
			this->fullName = child.def.args;
		} else if (child.def.name == "url") {
			this->url = child.def.args;
		} else if (child.def.name == "brief") {
			this->brief = child.def.args;
		} else {
			CDEPS_FATAL_ARG("Invalid command in package.infos : %1 at %2")
				.arg(child.def)
				.arg(child.def.source)
				.end();
		}
	}
}

/*******************  FUNCTION  *********************/
PackageExpectedFile::PackageExpectedFile(void)
{
	this->sendToVersionExtractor = false;
	this->optional = false;
	this->found = false;
}

/*******************  FUNCTION  *********************/
void PackageExpectedFile::loadFromTree(const SpecFileNode & node)
{
	//checks
	node.def.doAssume(node.def.type == SPEC_LINE_PATH,"Invalid de type, expect 'PATH'.");
	
	//extract path
	this->path = node.def.name;
	this->source = node.def.source;

	//loop on childs
	for (auto & child : node.childs) {
		if (child.def.type == SPEC_LINE_COMMAND) {
			this->loadCmdFromSpecLine(child.def);
		} else if (child.def.type == SPEC_LINE_NAME) {
			this->symbols.emplace_back();
			this->symbols.back().loadFromTree(child);
		} else {
			child.def.doFatal("Invalid entry in expected file in package.");
		}
	}
}

/*******************  FUNCTION  *********************/
void PackageExpectedFile::loadCmdFromSpecLine(const SpecFileLine & cmd)
{
	//check
	cmd.doAssume(cmd.type == SPEC_LINE_COMMAND,"Invalid type, expect COMMAND.");
	
	//switch
	if (cmd.name == "send-to-version-extractor") {
		this->sendToVersionExtractor = true;
	} else if (cmd.name == "optional") {
		this->optional = true;
		if (cmd.args.empty() == false)
			this->hasVars.push_back(cmd.args);
	} else if (cmd.name == "component") {
		cmd.requireArgs();
		this->components.push_back(cmd.args);
	} else if (cmd.name == "set") {
		cmd.requireArgs();
		this->setVars.push_back(ExportedVariable(cmd.args,false));
	} else if (cmd.name == "set-append") {
		cmd.requireArgs();
		this->setVars.push_back(ExportedVariable(cmd.args,true));
	} else if (cmd.name == "if") {
		cmd.requireArgs();
		this->ifHasVar.push_back(cmd.args);
	} else if (cmd.name == "dir") {
		cmd.requireArgs();
		this->extraSearchPaths.push_back(cmd.args);
	} else {
		cmd.doFatal("Invalid sub command on files.");
	}
}

/*******************  FUNCTION  *********************/
PackExpectedFileSymbol::PackExpectedFileSymbol(void)
{
	this->optional = false;
	this->found = false;
	this->type = CDEPS_SYMBOL_FUNCTION;
	this->lang = CDEPS_LANG_C;
}

/*******************  FUNCTION  *********************/
void PackExpectedFileSymbol::loadFromTree(const SpecFileNode & node)
{
	//checks
	node.def.doAssume(node.def.type == SPEC_LINE_NAME,"Invalid de type, expect 'NAME'.");
	
	//extract name
	this->name = node.def.name;
	this->source = node.def.source;

	//loop on childs
	for (auto & child : node.childs) {
		if (child.def.type == SPEC_LINE_COMMAND) {
			this->loadCmdFromSpecLine(child.def);
		} else {
			child.def.doFatal("Invalid entry in expected file in package.");
		}
	}
}

/*******************  FUNCTION  *********************/
void PackExpectedFileSymbol::loadCmdFromSpecLine(const SpecFileLine & cmd)
{
	//check
	cmd.doAssume(cmd.type == SPEC_LINE_COMMAND,"Invalid type, expect COMMAND.");
	
	//switch
	if (cmd.name == "optional") {
		this->optional = true;
		if (cmd.args.empty() == false)
			this->hasVars.push_back(cmd.args);
	} else if (cmd.name == "component") {
		cmd.requireArgs();
		this->components.push_back(cmd.args);
	} else if (cmd.name == "if") {
		cmd.requireArgs();
		this->ifHasVar.push_back(cmd.args);
	} else if (cmd.name == "lang") {
		cmd.requireArgs();
		this->lang = symbolLangFromString(cmd.args,cmd.source);
	} else if (cmd.name == "is") {
		cmd.requireArgs();
		this->type = symbolTypeFromString(cmd.args,cmd.source);
	} else if (cmd.name == "set") {
		cmd.requireArgs();
		this->setVars.push_back(ExportedVariable(cmd.args,false));
	} else if (cmd.name == "set-append") {
		cmd.requireArgs();
		this->setVars.push_back(ExportedVariable(cmd.args,true));
	} else {
		cmd.doFatal("Invalid sub command on files.");
	}
}

/*******************  FUNCTION  *********************/
PackageSymbolLang symbolLangFromString(const std::string & value, const SourceLine & source)
{
	if (value == "c" || value == "C") {
		return CDEPS_LANG_C;
	} else if (value == "cpp" || value == "CPP" || value == "c++") {
		return CDEPS_LANG_CPP;
	} else {
		CDEPS_FATAL_ARG("Invalid language type '%1' at %2").arg(value).arg(source).end();
		return CDEPS_LANG_C;
	}
}

/*******************  FUNCTION  *********************/
PackageSymbolType symbolTypeFromString(const std::string & value, const SourceLine & source)
{
	if (value == "func" || value == "function") {
		return CDEPS_SYMBOL_FUNCTION;
	} else if (value == "type") {
		return CDEPS_SYMBOL_TYPE;
	} else if (value == "var" || value == "variable") {
		return CDEPS_SYMBOL_VARIABLE;
	} else if (value == "macro") {
		return CDEPS_SYMBOL_MACRO;
	} else {
		CDEPS_FATAL_ARG("Invalid symbol type '%1' at %2").arg(value).arg(source).end();
		return CDEPS_SYMBOL_FUNCTION;
	}
}

}