cdeps
=====

`cdeps` is a tool to manage in one location the search of your C/C++ dependencies.

**This is a work in progress, NOT YET USABLE**

Description
-----------

`cdeps` is a tool to help seaching c/c++ depdendencies in a system also considering custom prefix.
First goal is to get a nice description of our requirement in a simple declarative file. 

Then `cdeps` can seach the files by itself to perform the search and requirement checks and provide
variables. This is usefull to use it in generic build tools.

`cdeps` also generate the cmake `FindXXXX.cmake` files so you can still distribute a standard cmake project
without being dependent to cdeps. It just help to ease the maintainance and boilate which you
always have when you maintain those files. It also help to have a stric and commin behavior between
all you `FindXXXX.cmake` files mostly by keeping the door open to provide an optional prefix
where to search a library which is often missing in lots of projects.

In future this might ease intagration with the configure wrapper script I'm using in all my cmake project
like the current one.

Discussion
----------

I consider cmake made a nice work in general compared to autotools. But I think they failed the same
way than autotools by not providing a declarative way to search dependencies (we of course need the
programmative way they have for corner cases). This is where `cdeps` tries to fill the gap as an external
and dedicated tool for this issue. Of course `pkg-config` could be a solution. But it required all the
existing packages to provide the `.pc` files which is a failed goal up to now. `cdeps` can rely on 
`pkg-config` when available but can of course work without it.

Dependencies
------------

In order to build you need :

* cmake >= 2.8.8
* C++ compiler compilant with C++11

In order to run you need :

* Nothing currently

Build
-----

In order to build you can use `cmake` or the wrapper script which provide a user interface close to `autotools`
and directly calling cmake :

```sh
#as we use cmake you cannot build in place
mkdir build
cd build
../configure --prefix=$HOME/usr
make
make test
make install
```

Usage
-----

TODO

Spec file example
-----------------

```sh
TODO
```

License
-------

`cdeps` is distributed under the CeCILL-C license which is fully LGPL compliant.
