#!/bin/bash
######################################################
#            PROJECT  : cdeps                        #
#            VERSION  : 0.0.0-dev                    #
#            DATE     : 11/2018                      #
#            LICENSE  : CeCILL-C                     #
######################################################

######################################################
BUILD_DIR=$(mktemp -d)
SOURCE_DIR=$PWD

######################################################
if [ ! -f dev/coverage.sh ]; then
	echo "You should run from root or source directory !" 1>&2
	exit 1
fi

######################################################
if ! which lcov &> /dev/null; then
	echo "You should install 'lcov' command !" 1>&2
	exit 1
fi

######################################################
if ! which genhtml &> /dev/null; then
	echo "You should install 'genhtml' (lcov) command !" 1>&2
	exit 1
fi

######################################################
set -e
set -x

######################################################
cd ${BUILD_DIR}

${SOURCE_DIR}/configure --enable-debug --enable-gcc-coverage
make
make test
lcov -o out.info -c -d .
lcov --remove out.info $(cat out.info | grep 'SF:' | cut -f 2 -d ':' | egrep "tests|google|/usr|tclap") -o out.info
genhtml -o ${SOURCE_DIR}/html-coverage out.info

######################################################
cd ${SOURCE_DIR}
rm -rfvd ${BUILD_DIR}

######################################################
echo
echo "DONE"