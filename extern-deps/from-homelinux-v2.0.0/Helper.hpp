/*****************************************************
             PROJECT  : homelinux
             VERSION  : 2.0.0
             DATE     : 06/2017
             AUTHOR   : Valat Sébastien
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef HL_HELPER_HPP
#define HL_HELPER_HPP

/********************  HEADERS  *********************/
//std
#include <string>
#include <functional>
#include <list>
#include <map>
#include <vector>
#include "../../src/common/Debug.hpp"

/*******************  TYPES  ************************/
//to avoid to include json.hpp everywhere
namespace Json
{
    class Value;
}

/*******************  NAMESPACE  ********************/
namespace hl
{

/*******************  TYPES  ************************/
typedef std::map<std::string,std::string> StringMap;
typedef std::map<std::string,std::list<std::string> > StringMapList;
typedef std::list<std::string> StringList;
typedef std::map<std::string,Json::Value> JsonMap;
typedef std::vector<std::string> StringVector;

#define forEach(type,it,list) for(type::iterator it = list.begin() ; it != list.end() ; ++it)
#define forEachConst(type,it,list) for(type::const_iterator it = list.begin() ; it != list.end() ; ++it)

/********************  STRUCT  **********************/
/**
 * Helper class providing basic functions used everywhere in homelinux.
**/
struct Helper
{
	static StringList split(const std::string & value,char separator,bool keepEmpty = false);
};

}

#endif //HL_HELPER_HPP
