/*****************************************************
             PROJECT  : homelinux
             VERSION  : 2.0.0
             DATE     : 06/2017
             AUTHOR   : Valat Sébastien
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <gtest/gtest.h>
#include "../Helper.hpp"

/***************** USING NAMESPACE ******************/
using namespace hl;

/*******************  FUNCTION  *********************/
TEST(Helper,stringSplit_1)
{
	std::string res;
	StringList lst = Helper::split("a1 a2  a3",' ');
	forEach(StringList,value,lst)
	{
		res += "(";
		res+=*value;
		res+=")";
	}
	EXPECT_EQ("(a1)(a2)(a3)",res);
}

/*******************  FUNCTION  *********************/
TEST(Helper,stringSplit_2)
{
	std::string res;
	StringList lst = Helper::split("a1 a2  a3    ",' ');
	forEach(StringList,value,lst)
	{
		res += "(";
		res+=*value;
		res+=")";
	}
	EXPECT_EQ("(a1)(a2)(a3)",res);
}
