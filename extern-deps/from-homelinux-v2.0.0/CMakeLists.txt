######################################################
#            PROJECT  : cdeps                        #
#            VERSION  : 0.0.0-dev                    #
#            DATE     : 10/2018                      #
#            LICENSE  : CeCILL-C                     #
######################################################

######################################################
include_directories(${CMAKE_BINARY_DIR}/src)

######################################################
include_directories(../)

######################################################
add_library(homelinux-portability OBJECT Regexp.cpp)
SET_TARGET_PROPERTIES(homelinux-portability PROPERTIES COMPILE_FLAGS -fPIC)

######################################################
add_library(homelinux-core OBJECT VersionMatcher.cpp Helper.cpp)
SET_TARGET_PROPERTIES(homelinux-core PROPERTIES COMPILE_FLAGS -fPIC)

######################################################
if (ENABLE_TESTS)
	add_subdirectory(tests)
endif (ENABLE_TESTS)
