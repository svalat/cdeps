/*****************************************************
             PROJECT  : homelinux
             VERSION  : 2.0.0
             DATE     : 06/2017
             AUTHOR   : Valat Sébastien
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
//std
#include "../../src/common/Debug.hpp"
#include "Helper.hpp"
#include <cstring>

/*******************  NAMESPACE  ********************/
namespace hl
{

/*******************  FUNCTION  *********************/
/**
* Helper function to split strings with element containing at most 4096 characters.
**/
StringList Helper::split(const std::string & value,char separator,bool keepEmpty)
{
	//vars
	char buffer[4096];
	StringList res;
	
	//read
	int cnt = 0;
	for (size_t i = 0 ; i < value.size() ; i++)
	{
		if (value[i] == separator && (cnt > 0 || keepEmpty))
		{
			buffer[cnt] = '\0';
			cnt = 0;
			res.push_back(buffer);
		} else if (value[i] != separator) {
			buffer[cnt++] = value[i];
			assume(cnt < 4096,"Split element is too large, should be shorter than 1024 characters !");
		}
	}
	
	//flush
	buffer[cnt] = '\0';
	if (cnt > 0)
		res.push_back(buffer);

	//ret
	return res;
}

}
