Extern dependencies
===================

`cdeps` reused sources from other projects which are all present in this directory and redistributed
with the changes under their original license.

googletest
----------

This is the library provided by google : https://github.com/google/googletest.

It is here because it is not available on most system which make the project easier to reuse.
I made no changes into the source code it is just there as is.

License is as original defined into the directory.

from-cern-lhcb-daqpipe
----------------------

This is a set of files I'm reusing between all my projects as infrastructure. The versions here
comes from my last project at CERN : https://gitlab.cern.ch/lhcb-daqpielhcb-daqpipe/lhcb-daqpipe-v2.

They are distributed under same license : CeCILL-C V1.
